package com.defenestrationcoding.snowtech.tileentities;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

import com.defenestrationcoding.snowtech.util.BlockPosition;
import com.defenestrationcoding.snowtech.util.Registration.SnowTechBlocks;
import com.defenestrationcoding.snowtech.util.WorldUtils;

/**
 * A part of the pump system. Animates the Falling Water block.
 * 
 * @author JavaMatrix
 * 
 */
public class TileEntityFallingWater extends TileEntity {
	// The pressure of this block.
	public int pressure = 1;
	public int tickCounter = 0;
	public int age = 0;
	public static int maxAge = 3600;

	/**
	 * Reads data from a NBT tag.
	 */
	public void readFromNBT(NBTTagCompound nbt) {
		// Read off the pressure value if it's present.
		super.readFromNBT(nbt);
		pressure = nbt.getInteger("pressure");
		age = nbt.getInteger("age");
	}

	/**
	 * Writes data to a NBT Tag.
	 */
	public void writeToNBT(NBTTagCompound nbt) {
		// Write the pressure value to the tag.
		super.writeToNBT(nbt);
		nbt.setInteger("pressure", pressure);
		nbt.setInteger("age", age);
	}

	/**
	 * Moves the falling water to a different place.
	 * 
	 * @param x
	 *            The X position to move to.
	 * @param y
	 *            The Y position to move to.
	 * @param z
	 *            The Z position to move to.
	 */
	public void moveTo(int x, int y, int z) {
		// Get rid of the current block.
		worldObj.setBlockToAir(xCoord, yCoord, zCoord);
		// Move it.
		worldObj.setBlock(x, y, z, SnowTechBlocks.blockFallingWater);
		// And move the te too.
		xCoord = x;
		yCoord = y;
		zCoord = z;
	}

	/**
	 * Updates the entity.
	 */
	public void updateEntity() {
		// Alrighty, this is going to be a long haul.
		// TL;DR It works on a pressure-based system, moving downwards
		// primarily, then sideways, then up.

		// First, make sure the world isn't remote.
		if (worldObj.isRemote) {
			return;
		}

		// Update the age counter.
		age++;
		if (age >= maxAge) {
			deleteThisTE();
		}

		// Now the tick counter, we only want to update every 5 ticks.
		tickCounter++;
		tickCounter %= 5;
		if (tickCounter != 0) {
			return;
		}

		// Let's make shorter variable names for the coords.
		int x = xCoord;
		int y = yCoord;
		int z = zCoord;

		// Ok, now down to business. First we check straight down for possible
		// actions, as gravity
		// shall prevail.
		Block down = worldObj.getBlock(x, y - 1, z);

		// See if it's air. If so, that's fairly simple.
		if (down.equals(Blocks.air)) {
			// Move straight down.
			moveTo(x, y - 1, z);
			// Return so it doesn't warp down mountains at light speed or
			// something.
			return;
		}

		// Before seeing if the block can compress into the water below it, it's
		// time to see if
		// it can move diagonally down to air.
		List<BlockPosition> diagonallyDownBlocks = new ArrayList<BlockPosition>();
		// Populate the list.
		// The + 0's are for formatting.
		diagonallyDownBlocks.add(new BlockPosition(x + 1, y - 1, z - 1));
		diagonallyDownBlocks.add(new BlockPosition(x + 1, y - 1, z + 0));
		diagonallyDownBlocks.add(new BlockPosition(x + 1, y - 1, z + 1));
		diagonallyDownBlocks.add(new BlockPosition(x - 1, y - 1, z - 1));
		diagonallyDownBlocks.add(new BlockPosition(x - 1, y - 1, z + 0));
		diagonallyDownBlocks.add(new BlockPosition(x - 1, y - 1, z + 1));
		diagonallyDownBlocks.add(new BlockPosition(x + 0, y - 1, z - 1));
		diagonallyDownBlocks.add(new BlockPosition(x + 0, y - 1, z + 1));

		// Now loop through those positions.
		for (BlockPosition pos : diagonallyDownBlocks) {
			// Grab the block at the position.
			Block diagDown = pos.getBlock(worldObj);
			// Check if it's air.
			if (diagDown.equals(Blocks.air)
					&& pos.getTop(worldObj).isReplaceable(worldObj, x, y, z)) {
				// Move it.
				moveTo(pos.x, pos.y, pos.z);
				// Return so we don't warp down mountains at light speed or
				// something.
				return;
			}
		}

		// First we check to see if it's air straight above this block.
		Block above = worldObj.getBlock(x, y + 1, z);

		// Now for the bit where we move to positions with less pressure.
		// First, down.
		if (down.equals(SnowTechBlocks.blockFallingWater)) {
			TileEntityFallingWater teDown = (TileEntityFallingWater) worldObj
					.getTileEntity(x, y - 1, z);
			// Check for lesser pressure.
			if (teDown.pressure < this.pressure) {
				// Transfer pressure to the te below this one.
				teDown.pressure++;
				this.pressure--;

				// And delete this if there's no pressure.
				if (this.pressure <= 0) {
					deleteThisTE();
				}

				// Return, since pressure interchange should go one step at a
				// time.
				return;
			}
		}

		// And now for the diagonally down pressure bit.
		for (BlockPosition pos : diagonallyDownBlocks) {
			// Grab the block and make sure it's another falling water block.
			if (pos.getBlock(worldObj).equals(SnowTechBlocks.blockFallingWater)
					&& pos.getTop(worldObj).isReplaceable(worldObj, x, y, z)) {
				// From here on out, it'll be just like the straight down bit.
				TileEntityFallingWater teDiagDown = (TileEntityFallingWater) pos
						.getTE(worldObj);
				if (teDiagDown.pressure < this.pressure) {
					teDiagDown.pressure++;
					this.pressure--;

					if (this.pressure <= 0) {
						deleteThisTE();
					}
				}

				return;
			}
		}

		// Now we need to equalize with water above us.
		if (above.equals(SnowTechBlocks.blockFallingWater)) {
			TileEntityFallingWater teAbove = (TileEntityFallingWater) (new BlockPosition(
					x, y + 1, z)).getTE(worldObj);
			// 1 atm needed to move upwards.
			if (teAbove.pressure < this.pressure - 1) {
				teAbove.pressure++;
				this.pressure--;
			}
		}

		// Now for the bit where we equalize pressure.
		// First we'll need a list of every stinking neighbor. Luckily for us,
		// WorldUtils has methods that can help.
		List<BlockPosition> neighbors = WorldUtils.buildNeighborList(worldObj,
				x, y, z, true);

		// Now we try and pop water to places where there's air nearby.
		List<BlockPosition> neighborAir = WorldUtils.weedBlocks(worldObj,
				neighbors, Blocks.air, true);

		// Let's loopy doo through the air blocks.
		for (BlockPosition pos : neighborAir) {
			if (this.pressure > 1) {
				// Remove upwards directions from the air positions if they're
				// there,
				// since that's a whole separate thing.
				if (pos.y == y) {
					// Make sure we follow the rules for diagonally downwards
					// flow.
					this.pressure--;
					worldObj.setBlock(pos.x, pos.y, pos.z,
							SnowTechBlocks.blockFallingWater);
				}
				if (!(pos.y < y && !pos.getTop(worldObj).isReplaceable(
						worldObj, x, y, z))) {
					this.pressure--;
					worldObj.setBlock(pos.x, pos.y, pos.z,
							SnowTechBlocks.blockFallingWater);
				}
			} else {
				break;
			}
		}

		// Now for equalizing to water with lower pressure.
		List<BlockPosition> neighborWater = WorldUtils.weedBlocks(worldObj,
				neighbors, SnowTechBlocks.blockFallingWater, true);

		// Check that the list isn't empty.
		if (neighborWater.size() > 0) {
			// Now loop through each one and push over as much pressure as
			// possible.
			for (BlockPosition pos : neighborWater) {
				// Grab the te.
				TileEntityFallingWater that = (TileEntityFallingWater) pos
						.getTE(worldObj);
				// Now find the average pressure.
				double averagePressure = (this.pressure + that.pressure) / 2.0;
				// And set the pressures. Any extra will stay here.
				that.pressure = (int) Math.floor(averagePressure);
				this.pressure = (int) Math.ceil(averagePressure);

				if (this.pressure == 1) {
					// Continuing after the pressure equals 1 could be
					// disastrous.
					return;
				}
			}
			// And end just in case.
			return;
		}

		// Now for the part where water shoots upwards.
		// We'll need some pressure to counteract gravity.
		if (above.equals(Blocks.air) && this.pressure >= 3) {
			// Up, up into the air!
			pressure--;
			worldObj.setBlock(x, y + 1, z, SnowTechBlocks.blockFallingWater);
		}

		// Note that there isn't any code for shooting up diagonally - this is
		// on purpose, I want
		// a fountain-like effect.

		// And finally, cause a minor explosion if the pressure is super high.
		if (pressure > 1000) {
			pressure -= 500;
			worldObj.createExplosion((Entity) null, (double) x, (double) y,
					(double) z, 2, true);
		}
	}

	public void deleteThisTE() {
		// Remove the block first.
		worldObj.setBlockToAir(xCoord, yCoord, zCoord);
		// Then this te.
		worldObj.removeTileEntity(xCoord, yCoord, zCoord);
	}
	

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);
		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, -999, nbt);
	}
	
	@Override
	public void onDataPacket(NetworkManager netManager, S35PacketUpdateTileEntity packet) {
		super.onDataPacket(netManager, packet);
		readFromNBT(packet.func_148857_g());
	}
}
