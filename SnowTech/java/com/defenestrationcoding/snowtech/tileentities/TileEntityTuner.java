package com.defenestrationcoding.snowtech.tileentities;

import com.defenestrationcoding.snowtech.util.HaywireUtils;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

public class TileEntityTuner extends TileEntity {
	public int signal = -1;
	
	@Override
	public void updateEntity() {
		HaywireUtils.doHWUpdate(worldObj, xCoord, yCoord, zCoord);
	}
	

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);
		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, -999, nbt);
	}
	
	@Override
	public void onDataPacket(NetworkManager netManager, S35PacketUpdateTileEntity packet) {
		super.onDataPacket(netManager, packet);
		readFromNBT(packet.func_148857_g());
	}
}
