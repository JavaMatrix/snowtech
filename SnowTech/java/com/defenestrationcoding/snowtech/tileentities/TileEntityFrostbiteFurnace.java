package com.defenestrationcoding.snowtech.tileentities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntitySnowman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.common.util.ForgeDirection;

public class TileEntityFrostbiteFurnace extends TileEntity implements
		IInventory {

	private static Map<Item, Integer> extra = new HashMap<Item, Integer>() {
		private static final long serialVersionUID = -2096610457891806612L;

		{
			put(Items.snowball, 200);
			put(Item.getItemFromBlock(Blocks.snow), 1000);
			put(Item.getItemFromBlock(Blocks.ice), 2500);
			put(Item.getItemFromBlock(Blocks.packed_ice), 7500);
		}
	};

	private ItemStack[] furnaceItems;
	public int furnaceBurnTime;
	public int currentItemBurnTime;
	public int furnaceCookTime;
	public int reduceAmount;
	public ItemStack currentSmeltingItem;
	public ItemStack currentBurningItem;
	public int smeltTime = 3200;
	public ForgeDirection front = ForgeDirection.NORTH;

	public int snowmen = 0;

	public static final int SLOT_REACTANT = 0;
	public static final int SLOT_PRODUCT = 1;
	public static final int SLOT_FUEL = 2;

	public TileEntityFrostbiteFurnace() {
		furnaceItems = new ItemStack[6];
		furnaceBurnTime = 0;
		currentItemBurnTime = 0;
		furnaceCookTime = 0;
		reduceAmount = 1;
	}

	public void writeToNBT(NBTTagCompound nbttagcompound) {
		super.writeToNBT(nbttagcompound);
		nbttagcompound.setShort("BurnTime", (short) furnaceBurnTime);
		nbttagcompound.setShort("CookTime", (short) furnaceCookTime);
		NBTTagList nbttaglist = new NBTTagList();
		for (int slot = 0; slot < furnaceItems.length; slot++) {
			if (furnaceItems[slot] != null) {
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) slot);
				furnaceItems[slot].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}
		nbttagcompound.setTag("Items", nbttaglist);
	}

	public void readFromNBT(NBTTagCompound nbttagcompound) {
		super.readFromNBT(nbttagcompound);

		NBTTagList nbtTagList = nbttagcompound.getTagList("Items", 0);

		furnaceItems = new ItemStack[getSizeInventory()];

		for (int i = 0; i < nbtTagList.tagCount(); i++) {
			NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbtTagList
					.getCompoundTagAt(i);

			byte slot = nbttagcompound1.getByte("Slot");
			if (slot >= 0 && slot < furnaceItems.length) {
				furnaceItems[slot] = ItemStack
						.loadItemStackFromNBT(nbttagcompound1);
			}
		}

		furnaceBurnTime = nbttagcompound.getShort("BurnTime");
		furnaceCookTime = nbttagcompound.getShort("CookTime");
		currentItemBurnTime = getItemBurnTime(furnaceItems[1]);
		currentSmeltingItem = furnaceItems[3];
	}

	private int getItemBurnTime(ItemStack itemStack) {
		if (!isFuel(itemStack)) {
			return 0;
		}

		return extra.get(itemStack.getItem());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateEntity() {
		boolean burning = isBurning();
		boolean changed = false;

		smeltTime = 3200;

		AxisAlignedBB box = AxisAlignedBB.getBoundingBox(xCoord - 8,
				yCoord - 8, zCoord - 8, xCoord + 8, yCoord + 8, zCoord + 8);

		List<Entity> entities = worldObj.getEntitiesWithinAABB(
				EntitySnowman.class, box);

		for (Entity e : entities) {
			e.attackEntityFrom(DamageSource.onFire, 0.1F);
		}

		int snowGolems = entities.size();

		snowGolems = Math.min(snowGolems, 31);
		smeltTime -= snowGolems * 100;
		snowmen = snowGolems;

		if (burning) {
			furnaceBurnTime--;
		}

		if (!worldObj.isRemote) {
			if (furnaceBurnTime == 0 && (canSmelt())) {
				int time = getItemBurnTime(furnaceItems[SLOT_FUEL]);
				time = (int) ((float) time * 0.79f);

				furnaceBurnTime = currentItemBurnTime = time;
				currentSmeltingItem = furnaceItems[5] = furnaceItems[SLOT_FUEL];

				if (BiomeDictionary.isBiomeOfType(
						worldObj.getBiomeGenForCoords(xCoord, zCoord),
						Type.COLD)) {
					furnaceBurnTime += smeltTime;
					currentItemBurnTime += smeltTime;
				}

				if (time > 0) {
					changed = true;
					if (furnaceItems[SLOT_FUEL] != null) {
						furnaceItems[SLOT_FUEL].stackSize--;
						if (furnaceItems[SLOT_FUEL].stackSize == 0) {
							furnaceItems[SLOT_FUEL] = null;
						}
					}
				}
			}

			if (isBurning()) {
				if (canSmelt()) {
					furnaceCookTime++;
					if (furnaceCookTime >= smeltTime) {
						smeltItem();
						furnaceCookTime = 0;
						changed = true;
					}
				} else {
					furnaceCookTime = 0;
				}
			} else {
				furnaceCookTime = 0;
			}

			if (burning != isBurning()) {
				changed = true;
				// BlockFrostbiteFurnace.updateFurnaceBlockState(isBurning(),
				// worldObj, xCoord, yCoord, zCoord);
			}

			if (changed) {
				this.markDirty();
				int meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
				worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord,
						meta, 3);
			}
		}
	}

	private void smeltItem() {
		if (!canSmelt()) {
			return;
		}

		ItemStack itemstack = FurnaceRecipes.smelting().getSmeltingResult(
				this.furnaceItems[SLOT_REACTANT]);

		if (this.furnaceItems[SLOT_PRODUCT] == null) {
			this.furnaceItems[SLOT_PRODUCT] = itemstack.copy();
		} else if (this.furnaceItems[SLOT_PRODUCT].getItem() == itemstack
				.getItem()) {
			this.furnaceItems[SLOT_PRODUCT].stackSize += itemstack.stackSize;
		}

		--this.furnaceItems[SLOT_REACTANT].stackSize;

		if (this.furnaceItems[SLOT_REACTANT].stackSize <= 0) {
			this.furnaceItems[SLOT_REACTANT] = null;
		}
	}

	private boolean canSmelt() {
		if (this.furnaceItems[SLOT_REACTANT] == null) {
			return false;
		} else {
			ItemStack itemstack = FurnaceRecipes.smelting().getSmeltingResult(
					this.furnaceItems[SLOT_REACTANT]);
			if (itemstack == null)
				return false;
			if (this.furnaceItems[SLOT_PRODUCT] == null)
				return true;
			if (!this.furnaceItems[SLOT_PRODUCT].isItemEqual(itemstack))
				return false;
			int result = furnaceItems[SLOT_PRODUCT].stackSize
					+ itemstack.stackSize;
			return result <= getInventoryStackLimit()
					&& result <= this.furnaceItems[SLOT_PRODUCT]
							.getMaxStackSize();
		}
	}

	public static boolean isFuel(ItemStack items) {
		if (items == null) {
			return false;
		}

		return extra.containsKey(items.getItem());
	}

	public boolean isBurning() {
		return furnaceBurnTime > 0;
	}

	public int getBurnTimeRemainingScaled(int i) {
		if (currentItemBurnTime == 0) {
			return 0;
		}

		return (furnaceBurnTime * i) / currentItemBurnTime;
	}

	public int getCookProgressScaled(int i) {
		return furnaceCookTime * i / smeltTime;
	}

	@Override
	public int getSizeInventory() {
		return furnaceItems.length;
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		return furnaceItems[slot];
	}

	@Override
	public ItemStack decrStackSize(int slot, int amount) {
		if (furnaceItems[slot] != null) {
			if (furnaceItems[slot].stackSize <= amount) {
				ItemStack itemstack = furnaceItems[slot];
				furnaceItems[slot] = null;
				return itemstack;
			}
			ItemStack itemstack1 = furnaceItems[slot].splitStack(amount);
			if (furnaceItems[slot].stackSize == 0) {
				furnaceItems[slot] = null;
			}
			return itemstack1;
		} else {
			return null;
		}
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot) {
		if (this.furnaceItems[slot] != null) {
			ItemStack items = this.furnaceItems[slot];
			this.furnaceItems[slot] = null;
			return items;
		} else {
			return null;
		}
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack val) {
		furnaceItems[slot] = val;
		if (val != null && val.stackSize > getInventoryStackLimit()) {
			val.stackSize = getInventoryStackLimit();
		}
	}

	@Override
	public String getInventoryName() {
		return "Frostbite Furnace";
	}

	@Override
	public boolean hasCustomInventoryName() {
		return true;
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		return this.worldObj.getTileEntity(this.xCoord, this.yCoord,
				this.zCoord) != this ? false : entityplayer.getDistanceSq(
				(double) this.xCoord + 0.5D, (double) this.yCoord + 0.5D,
				(double) this.zCoord + 0.5D) <= 64.0D;
	}

	@Override
	public void openInventory() {
	}

	@Override
	public void closeInventory() {
	}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack items) {
		return false;
	}

	public static ItemStack smeltResult(ItemStack items) {
		return FurnaceRecipes.smelting().getSmeltingResult(items);
	}

	public int getPercentDone() {

		if (smeltTime == 0) {
			return -1;
		}

		float decimal = (float) furnaceCookTime / (float) smeltTime;
		decimal *= 100;
		return (int) decimal;
	}


	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);
		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, -999, nbt);
	}
	
	@Override
	public void onDataPacket(NetworkManager netManager, S35PacketUpdateTileEntity packet) {
		super.onDataPacket(netManager, packet);
		readFromNBT(packet.func_148857_g());
	}
}
