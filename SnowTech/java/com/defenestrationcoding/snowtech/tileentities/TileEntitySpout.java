package com.defenestrationcoding.snowtech.tileentities;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.defenestrationcoding.snowtech.util.BlockPosition;
import com.defenestrationcoding.snowtech.util.Registration.SnowTechBlocks;

/**
 * This tile entity serves to update the spout, helping it convert pumped water
 * to real water.
 * 
 * @author JavaMatrix
 * 
 */
public class TileEntitySpout extends TileEntity {
	public int updateClock = 0;
	public List<BlockPosition> pond = new ArrayList<BlockPosition>();
	
	// Internal buffer in buckets: a bucket is equal to 1000 millibuckets.
	public int bufferInBuckets = 0;

	// Whether a valid pond has been build.
	public boolean isValidPond = false;

	// -1: No pond detected
	// 0: No problem
	// 1: Pond is too big or unbounded.
	// 2: Pond is full.
	// 3: No water to pour into pond.
	public int problemCode = -1;
	
	BlockPosition front;
	BlockPosition back;

	/**
	 * A recursive function for building pond objects.
	 * 
	 * @param world
	 *            The world in which the pump is running.
	 * @param x
	 *            The x position to start from.
	 * @param y
	 *            The y position to start from.
	 * @param z
	 *            The z position to start from.
	 * @param depth
	 *            The current recursive depth.
	 * @return Whether the scan successfully created a valid pond.
	 */
	public boolean scan(World world, int x, int y, int z, int depth) {
		// Don't run in infinite loops.
		if (pond.contains(new BlockPosition(x, y, z))) {
			return true;
		}

		// Ponds shouldn't expand beyond 32 blocks manhattan distance from the
		// spout.
		if (depth > 32) {
			return false;
		}

		// Ponds can include air or water.
		if (world.getBlock(x, y, z).equals((Blocks.air))
				|| world.getBlock(x, y, z).equals(Blocks.water)) {
			// Add the block to the pond list.
			// Water blocks get removed in buildPond, but we'll add them for now
			// to allow the spout to finish partial ponds.
			pond.add(new BlockPosition(x, y, z));

			// And recursively scan neighboring blocks (Von Nuemann neighborhood
			// only)
			scan(world, x + 1, y, z, depth + 1);
			scan(world, x - 1, y, z, depth + 1);
			scan(world, x, y - 1, z, depth + 1);
			scan(world, x, y, z + 1, depth + 1);
			scan(world, x, y, z - 1, depth + 1);
		}
		return true;
	}

	/**
	 * Determines where the front and back of the block are.
	 * 
	 * @param world
	 */
	public void recalcSides(World world) {
		// Start at the block's own position.
		front = new BlockPosition(xCoord, yCoord, zCoord);
		back = new BlockPosition(xCoord, yCoord, zCoord);

		// Then, based on the metadata of the block, move the front and back 1
		// block in
		// the right direction.
		// Directions are represented as {up, down, north, east, south, west}.
		switch (world.getBlockMetadata(xCoord, yCoord, zCoord)) {
		case 0:
			front.y++;
			back.y--;
			break;
		case 1:
			front.y--;
			back.y++;
			break;
		case 2:
			front.z--;
			back.z++;
			break;
		case 3:
			front.x++;
			back.x--;
			break;
		case 4:
			front.z++;
			back.z--;
			break;
		case 5:
			front.x--;
			back.x++;
			break;
		}
	}

	/**
	 * Builds a list of blocks to be filled with water (a pond).
	 * 
	 * @param world
	 *            The world that the block is in.
	 * @return True if the pond is valid, false if it is not.
	 */
	public boolean buildPond(World world) {
		// Find the front and back of the block.
		recalcSides(world);

		// Clear any residual pond blocks from the last pond.
		pond.clear();

		// Recursively scan for pond candidates.
		boolean success = scan(world, front.x, front.y, front.z, 1);

		// Remove any water blocks, and count them.
		int filled = 0;
		for (BlockPosition pos : new ArrayList<BlockPosition>(pond)) {
			if (pos.getBlock(world).equals(Blocks.water)) {
				pond.remove(pos);
				filled++;
			}
		}

		// Determine the proper error code from the pond state.
		if (pond.size() >= 1 && success) {
			isValidPond = true;
			problemCode = 0;
		} else if (!success) {
			isValidPond = false;
			problemCode = 1;
		} else {
			isValidPond = false;
			// Either there is no pond, or the pond is full.
			// We will check for water blocks in the pond area to determine
			// which is which.
			if (filled == 0) {
				problemCode = -1;
			} else {
				problemCode = 2;
			}
		}

		return isValidPond;
	}

	@Override
	public void updateEntity() {
		// Only update once every 20 ticks (1 Hz).
		updateClock++;
		updateClock %= 20;

		// Only update if there's a pond to fill and it's time to update.
		if (updateClock == 0 && isValidPond) {
			// Also make sure that the pond isn't full.
			if (pond.size() > 0) {
				// Only add water to the pond if there's water in the buffer.
				if (bufferInBuckets > 0) {
					// Find a position to fill.
					BlockPosition toFill = pond.get(0);
					// Remove it from the empty pond block cache.
					pond.remove(toFill);
					// Set the block to water.
					toFill.setBlock(worldObj, Blocks.water);
					// Remove the water from the internal buffer.
					bufferInBuckets--;
				}
				// Check for pumped water behind this block to bring into the
				// internal buffer.
				if (back.getBlock(worldObj).equals(
						SnowTechBlocks.blockFallingWater)) {
					// Get the TE at the position behind the block.
					TileEntityFallingWater te = (TileEntityFallingWater) back
							.getTE(worldObj);
					// Move 1 bucket (1000 mB) of water from the pumped water to
					// the internal buffer.
					bufferInBuckets++;
					te.pressure--;

					// If the TE is out of water, remove it from the world.
					if (te.pressure <= 0) {
						back.deleteBlock(worldObj);
					}
				} else if (bufferInBuckets <= 0) {
					// Problem Code 3, no water to pour.
					problemCode = 3;
				}
			} else {
				// Problem Code 2, pond is full.
				problemCode = 2;
			}
		} else if (updateClock == 0 && !isValidPond) {
			// Try detecting a new pond.
			buildPond(worldObj);
		}
	}

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);
		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, -999, nbt);
	}
	
	@Override
	public void onDataPacket(NetworkManager netManager, S35PacketUpdateTileEntity packet) {
		super.onDataPacket(netManager, packet);
		readFromNBT(packet.func_148857_g());
	}
}
