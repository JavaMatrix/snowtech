package com.defenestrationcoding.snowtech.tileentities;

import java.util.HashMap;
import java.util.Map;

import javax.vecmath.Vector3f;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;

import com.defenestrationcoding.snowtech.IHaywireDevice;
import com.defenestrationcoding.snowtech.util.HaywireUtils;

public class TileEntityHaywire extends TileEntity implements IHaywireDevice {
	public boolean[] connections = { false, false, false, false, false, false };
	public int signal = -1;
	public long lastSignalUpdate = 0;

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setInteger("signal", signal);
		nbt.setLong("lastSignalUpdate", lastSignalUpdate);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		signal = nbt.getInteger("signal");
		lastSignalUpdate = nbt.getLong("lastSignalUpdate");
	}
	
	public void updateConnections() {
		connections = new boolean[] { false, false, false, false, false, false };
		Map<Vector3f, ForgeDirection> neighbors = new HashMap<Vector3f, ForgeDirection>();
		neighbors.put(new Vector3f(xCoord - 1, yCoord, zCoord),
				ForgeDirection.SOUTH);
		neighbors.put(new Vector3f(xCoord + 1, yCoord, zCoord),
				ForgeDirection.NORTH);
		neighbors.put(new Vector3f(xCoord, yCoord - 1, zCoord),
				ForgeDirection.DOWN);
		neighbors.put(new Vector3f(xCoord, yCoord + 1, zCoord),
				ForgeDirection.UP);
		neighbors.put(new Vector3f(xCoord, yCoord, zCoord - 1),
				ForgeDirection.EAST);
		neighbors.put(new Vector3f(xCoord, yCoord, zCoord + 1),
				ForgeDirection.WEST);

		for (Vector3f where : neighbors.keySet()) {
			TileEntity b = worldObj.getTileEntity((int) where.x, (int) where.y,
					(int) where.z);
			if (b instanceof IHaywireDevice) {
				IHaywireDevice d = (IHaywireDevice) b;
				if (d.shouldConnectOnSide(neighbors.get(where).getOpposite())) {
					connections[neighbors.get(where).ordinal()] = true;
				}
			}
		}
	}
	
	@Override
	public void updateEntity() {
		HaywireUtils.doHWUpdate(worldObj, xCoord, yCoord, zCoord);
	}

	@Override
	public int getHWSignal() {
		return signal;
	}

	@Override
	public void setHWSignal(int value) {
		signal = value;
		lastSignalUpdate = System.currentTimeMillis();
	}

	@Override
	public long getUpdateRank() {
		return lastSignalUpdate;
	}

	@Override
	public boolean shouldConnectOnSide(ForgeDirection side) {
		return true;
	}
	

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);
		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, -999, nbt);
	}
	
	@Override
	public void onDataPacket(NetworkManager netManager, S35PacketUpdateTileEntity packet) {
		super.onDataPacket(netManager, packet);
		readFromNBT(packet.func_148857_g());
	}
}
