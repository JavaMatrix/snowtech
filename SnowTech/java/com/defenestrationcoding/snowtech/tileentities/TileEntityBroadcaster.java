package com.defenestrationcoding.snowtech.tileentities;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;

import com.defenestrationcoding.snowtech.IHaywireDevice;
import com.defenestrationcoding.snowtech.util.HaywireUtils;

public class TileEntityBroadcaster extends TileEntity implements IHaywireDevice {
	public int signal = -1;
	public int sliderX = -1;
	public long lastSignalUpdate = Long.MAX_VALUE; // Super blast should
													// broadcast to all haywires
	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setInteger("signal", signal);
		nbt.setInteger("sliderX", sliderX);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		
		signal = nbt.getInteger("signal");
		sliderX = nbt.getInteger("sliderX");
	}
	
	@Override
	public void updateEntity() {
		HaywireUtils.doHWUpdate(worldObj, xCoord, yCoord, zCoord);
	}

	@Override
	public int getHWSignal() {
		return signal;
	}

	@Override
	public void setHWSignal(int value) {
		return;
	}

	@Override
	public long getUpdateRank() {
		return Long.MAX_VALUE;
	}

	@Override
	public boolean shouldConnectOnSide(ForgeDirection side) {
		return true;
	}

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);
		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, -999, nbt);
	}
	
	@Override
	public void onDataPacket(NetworkManager netManager, S35PacketUpdateTileEntity packet) {
		super.onDataPacket(netManager, packet);
		readFromNBT(packet.func_148857_g());
	}
}
