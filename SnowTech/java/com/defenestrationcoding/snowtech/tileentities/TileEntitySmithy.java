package com.defenestrationcoding.snowtech.tileentities;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

import org.lwjgl.Sys;

import com.defenestrationcoding.snowtech.util.BlockPosition;
import com.defenestrationcoding.snowtech.util.Registration.SnowTechItems;
import com.defenestrationcoding.snowtech.util.WorldUtils;

public class TileEntitySmithy extends TileEntity {

	public static List<Material> heatSources = new ArrayList<Material>() {
		private static final long serialVersionUID = -1527238290724001695L;

		{
			add(Material.fire);
			add(Material.lava);
		}
	};

	// 0 = No ore, not doing anything.
	// 1 = Melting ore
	// 2 = Molten ingot, hammering.
	// 3 = Improving metal
	public int state = 0;

	// 0 = No metal
	// 1 = Iron (tough metal)
	// 2 = Gold (glittering metal)
	public int metalType = 0;

	// Melting takes 100 progress ticks (5 seconds) to complete.
	// Molten ingots take 50 hits to turn to a usable ingot.
	// Every hit after that will improve metal quality.
	public float progress = 0;

	public int burnTime = 0;

	public int cooldown = 0;

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setInteger("burnTime", burnTime);
		nbt.setInteger("state", state);
		nbt.setInteger("metalType", metalType);
		nbt.setFloat("progress", progress);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		burnTime = nbt.getInteger("burnTime");
		state = nbt.getInteger("state");
		metalType = nbt.getInteger("metalType");
		progress = nbt.getInteger("progress");
	}

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);
		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, -999, nbt);
	}

	@Override
	public void onDataPacket(NetworkManager netManager,
			S35PacketUpdateTileEntity packet) {
		super.onDataPacket(netManager, packet);
		readFromNBT(packet.func_148857_g());
	}

	@Override
	public void updateEntity() {
		cooldown--;

		List<BlockPosition> neumannNeighbors = WorldUtils.buildNeighborList(
				worldObj, xCoord, yCoord, zCoord, false);

		boolean hasSource = false;

		for (BlockPosition pos : neumannNeighbors) {
			if (heatSources.contains(pos.getBlock(worldObj).getMaterial())) {
				hasSource = true;
				
				if (pos.getBlock(worldObj).getMaterial() == Material.lava) {
					int chance = 6000;
					List<BlockPosition> neighbors = WorldUtils.buildNeighborList(worldObj, pos.x, pos.y, pos.z, true);
					for (BlockPosition block : neighbors) {
						if (block.getBlock(worldObj).getMaterial() == Material.lava) {
							chance *= 1.2;
						}
					}
					Random r = new Random(Sys.getTime());
					if (r.nextInt(chance) == 314) {
						pos.setBlock(worldObj, Blocks.cobblestone);
					}
				}
			}
		}

		if (hasSource) {
			burnTime++;
		}

		if (state == 1) {
			if (burnTime > 0) {
				progress++;
				if (progress >= 200) {
					state = 2;
					progress = 0;
				}
			} else {
				if (progress > 0) {
					progress--;
				} else {
					pop();
				}
			}
		} else if (state == 2) {
			if (burnTime > 0) {
				if (progress >= 50) {
					state = 3;
					progress = 0;
				}
			} else {
				pop();
			}
		} else if (state == 3) {
			if (burnTime <= 0) {
				pop();
			}
			if (progress >= 2000) {
				pop();
			}
		}

		if (burnTime > 0 || hasSource) {
			worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 1, 3);
			if (burnTime > 0) {
				burnTime--;
			}
		} else {
			worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 0, 3);
		}

	}

	public void pop() {
		if (!worldObj.isRemote) {
			Item toPop = SnowTechItems.itemDebugger;
			int baseDurability = 0;
			if (state == 3) {
				if (metalType == 1) {
					toPop = SnowTechItems.hardenedMetalIngot;
					baseDurability = 85;
				} else if (metalType == 2) {
					toPop = SnowTechItems.hardenedMagicalIngot;
					baseDurability = 10;
				} else {
					System.out.println(metalType);
				}
			} else if (state == 1) {
				if (metalType == 1) {
					toPop = Item.getItemFromBlock(Blocks.iron_ore);
				} else {
					toPop = Item.getItemFromBlock(Blocks.gold_ore);
				}
			} else {
				toPop = Items.flint;
			}
			EntityItem e = new EntityItem(worldObj, xCoord, yCoord + 1, zCoord,
					new ItemStack(toPop, 1, baseDurability + (int) progress));
			e.motionX = 0;
			e.motionY = 0.1;
			e.motionZ = 0;
			worldObj.spawnEntityInWorld(e);
		}
		this.state = 0;
		this.metalType = 0;
	}
}
