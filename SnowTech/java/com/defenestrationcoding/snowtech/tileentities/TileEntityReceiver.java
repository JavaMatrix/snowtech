package com.defenestrationcoding.snowtech.tileentities;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;

import com.defenestrationcoding.snowtech.IHaywireDevice;
import com.defenestrationcoding.snowtech.util.HaywireUtils;

public class TileEntityReceiver extends TileEntity implements IHaywireDevice {
	public Class<? extends IHaywireDevice> sender;
	public int x,y,z;
	public int signal = -1;
	public long lastSignalUpdate = 0;
	
	@Override
	public void updateEntity() {
		HaywireUtils.doHWUpdate(worldObj, xCoord, yCoord, zCoord);
	}

	@Override
	public int getHWSignal() {
		return -1;
	}

	@Override
	public void setHWSignal(int value) {
		signal = value;
		lastSignalUpdate = 0;
	}

	@Override
	public long getUpdateRank() {
		return lastSignalUpdate;
	}

	@Override
	public boolean shouldConnectOnSide(ForgeDirection side) {
		return true;
	}

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);
		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, -999, nbt);
	}
	
	@Override
	public void onDataPacket(NetworkManager netManager, S35PacketUpdateTileEntity packet) {
		super.onDataPacket(netManager, packet);
		readFromNBT(packet.func_148857_g());
	}
}
