package com.defenestrationcoding.snowtech.containers;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;

/**
 * Just an empty class to allow for a GUI in the
 * research table.
 * 
 * @author JavaMatrix
 *
 */
public class ContainerResearchTable extends Container {

	@Override
	public boolean canInteractWith(EntityPlayer var1) {
		return true;
	}

}
