package com.defenestrationcoding.snowtech.containers;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;

/**
 * This class doesn't do anything but allow for a GUI.
 * @author JavaMatrix
 *
 */
public class ContainerBroadcaster extends Container {

	// Just a dummy class to allow for a gui.
	
	@Override
	public boolean canInteractWith(EntityPlayer var1) {
		return true;
	}

}
