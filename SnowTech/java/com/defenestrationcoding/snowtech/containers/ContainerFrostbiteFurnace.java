package com.defenestrationcoding.snowtech.containers;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;

import com.defenestrationcoding.snowtech.tileentities.TileEntityFrostbiteFurnace;

/**
 * The container class for the Frostbite Furnace.
 * 
 * @author JavaMatrix
 * 
 */
public class ContainerFrostbiteFurnace extends Container {
	// The tile entity bound to this container.
	private TileEntityFrostbiteFurnace te;

	// A variable to let the GUI know how much longer the
	// furnace will be burning.
	public int furnaceBurnTime = 0;
	// How much longer the current item will burn for.
	public int currentItemBurnTime = 0;
	// How long it takes the furnace to "cook" an item.
	public int furnaceCookTime = 0;

	// Constants for easy access to specific slots.
	public static final int SLOT_REACTANT = 0;
	public static final int SLOT_PRODUCT = 1;
	public static final int SLOT_FUEL = 2;

	// Packet constants denoting what kind of packet is being sent
	// between the TE and the Container.
	public static final int PACKET_FURNACE_BURN_TIME = 0;
	public static final int PACKET_CURRENT_ITEM_BURN_TIME = 1;
	public static final int PACKET_FURNACE_COOK_TIME = 2;

	public ContainerFrostbiteFurnace(InventoryPlayer playerInv,
			TileEntityFrostbiteFurnace teff) {
		// Set the Tile Entity
		this.te = teff;

		// Make the reactant, fuel, and product slots
		addSlotToContainer(new Slot(teff, SLOT_REACTANT, 56, 17));
		addSlotToContainer(new Slot(teff, SLOT_FUEL, 56, 53));
		addSlotToContainer(new SlotFurnace(playerInv.player, teff,
				SLOT_PRODUCT, 116, 35));

		// Add the player inventory
		for (int i = 0; i < 3; i++) {
			for (int k = 0; k < 9; k++) {
				this.addSlotToContainer(new Slot(playerInv, k + i * 9 + 9,
						8 + k * 18, 84 + i * 18));
			}
		}

		// And the quickbar
		for (int j = 0; j < 9; j++) {
			this.addSlotToContainer(new Slot(playerInv, j, 8 + j * 18, 142));
		}
	}

	@Override
	public void addCraftingToCrafters(ICrafting crafting) {
		super.addCraftingToCrafters(crafting);

		// Send over the data needed to update the progress bar
		crafting.sendProgressBarUpdate(this, PACKET_FURNACE_BURN_TIME,
				furnaceBurnTime);
		crafting.sendProgressBarUpdate(this, PACKET_CURRENT_ITEM_BURN_TIME,
				currentItemBurnTime);
		crafting.sendProgressBarUpdate(this, PACKET_FURNACE_COOK_TIME,
				furnaceCookTime);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();

		// Notify all listeners of changes
		for (ICrafting listener : (List<ICrafting>) crafters) {
			// Check for furnace burn time update
			if (this.furnaceBurnTime != te.furnaceBurnTime) {
				listener.sendProgressBarUpdate(this, 0, te.furnaceBurnTime);
			}

			// Check for item burn time update
			if (this.currentItemBurnTime != te.currentItemBurnTime) {
				listener.sendProgressBarUpdate(this, 1, te.currentItemBurnTime);
			}

			// Check for cook time update
			if (this.furnaceCookTime != te.furnaceCookTime) {
				listener.sendProgressBarUpdate(this, 2, te.furnaceCookTime);
			}
		}

		// Finally, change the values in the container.
		this.furnaceBurnTime = te.furnaceBurnTime;
		this.currentItemBurnTime = te.currentItemBurnTime;
		this.furnaceCookTime = te.furnaceCookTime;
	}

	@Override
	public void updateProgressBar(int key, int val) {
		// Choose the right key, then update the TileEntity accordingly.
		switch (key) {
		case PACKET_FURNACE_BURN_TIME:
			te.furnaceBurnTime = val;
			break;
		case PACKET_CURRENT_ITEM_BURN_TIME:
			te.currentItemBurnTime = val;
			break;
		case PACKET_FURNACE_COOK_TIME:
			te.furnaceCookTime = val;
			break;
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer player) {
		// Pass it over to the TE.
		return te.isUseableByPlayer(player);
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slotId) {
		// Give a null result by default.
		ItemStack result = null;

		// Grab the slot in question
		Slot slot = (Slot) inventorySlots.get(slotId);

		// Check if the slot has an item in it.
		if (slot != null && slot.getHasStack()) {
			// Grab the item stack in the slot, and copy it for our result.
			ItemStack items = slot.getStack();
			result = items.copy();

			// Check for special conditions based on slot.
			if (slotId == SLOT_PRODUCT) {
				// Try merging the stack
				if (!mergeItemStack(items, 3, 39, true)) { // Haven't a clue
															// what
															// the 3 and 39 do
					// Error, I guess?
					return null;
				}
			} else if (slotId != SLOT_REACTANT && slotId != SLOT_FUEL) {
				// Must be SLOT_PRODUCT
				// Is it a smeltable?
				if (TileEntityFrostbiteFurnace.smeltResult(items) != null) {
					// Smeltable, put it in the reactant slot.
					if (!this.mergeItemStack(items, 0, 1, false)) {
						// Error, I guess?
						return null;
					} else if (TileEntityFrostbiteFurnace.isFuel(items)) { // Is
																			// it
																			// fuel?
						// It's fuel, put it in the fuel slot.
						if (!this.mergeItemStack(items, 1, 2, false)) {
							return null;
						}
					} else if (slotId >= 3 && slotId < 30) { // It must be from
																// player
																// inventory
						// Put it in the quickbar
						if (!this.mergeItemStack(items, 30, 39, false)) {
							return null;
						}
					} else if (slotId >= 30 && slotId <= 39) { // Hotbar
						if (!mergeItemStack(items, 3, 30, false)) {
							// Error, I guess?
							return null;
						}
						// Who knows where it's from? Just mash it in.
					} else if (!mergeItemStack(items, 3, 39, false)) {
						return null;
					}

					if (items.stackSize == 0) { // No moar itemz
						slot.putStack((ItemStack) null);
					} else {
						slot.onSlotChanged();
					}
					if (items.stackSize == result.stackSize) { // ?
						return null;
					}
					slot.onPickupFromSlot(player, items);
				}
			}

		}

		return result;
	}
}
