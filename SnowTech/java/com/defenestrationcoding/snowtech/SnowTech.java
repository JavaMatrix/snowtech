package com.defenestrationcoding.snowtech;

import net.minecraftforge.common.MinecraftForge;
import tconstruct.tools.entity.FancyEntityItem;

import com.defenestrationcoding.snowtech.compat.NEICompat;
import com.defenestrationcoding.snowtech.gui.GuiHandler;
import com.defenestrationcoding.snowtech.proxy.CommonProxy;
import com.defenestrationcoding.snowtech.util.Crafting;
import com.defenestrationcoding.snowtech.util.Registration;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.registry.EntityRegistry;

/**
 * The main mod class - it defines all of the items, blocks, and tileentities,
 * as well as plugging in hooks (to mix metaphors), adding recipes, and handling
 * research.
 * 
 * @author JavaMatrix
 * 
 */
@Mod(modid = SnowTech.modid, version = SnowTech.version, name = SnowTech.name)
public class SnowTech {

	// Basic mod data.
	public static final String modid = "snowtech";
	public static final String version = "1.7b1";
	public static final String name = "SnowTech";
	public static final boolean researchDisabled = true;

	// Where configs are stored.
	public static String configPath = "";

	// The proxy that handles rendering stuff.
	@SidedProxy(serverSide = "com.defenestrationcoding.snowtech.proxy.CommonProxy", clientSide = "com.defenestrationcoding.snowtech.proxy.ClientProxy")
	public static CommonProxy proxy;

	// This is a singleton, after all...
	@Instance("snowtech")
	public static SnowTech instance;

	/**
	 * Happens before initialization. Used to fetch the config path, register
	 * blocks/items, and initialize the GuiHandler and the proxy.
	 * 
	 * @param event
	 *            The event being handled.
	 */
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		// Grab the config path.
		configPath = event.getModConfigurationDirectory().getAbsolutePath();

		// Register items and blocks and entities.
		Registration.registerItems();
		Registration.registerBlocks();
		Registration.registerEntities();

		// Init the GuiHandler.
		GuiHandler.init();

		// Register the items.
		proxy.registerRenderers();
	}

	/**
	 * Called as FML initializes Minecraft. Used to make recipes and register
	 * event hooks.
	 * 
	 * @param event
	 *            The event being handled.
	 */
	@EventHandler
	public void init(FMLInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(new SnowTechHooks());
		FMLCommonHandler.instance().bus().register(new SnowTechFMLHooks());

		// Hand over recipe stuff to Registration.
		Registration.registerRecipes();

		// Soft depends for NEI
		if (Loader.isModLoaded("NotEnoughItems")) {
			NEICompat.load();
		}

		// Register the FancyEntityItem entity.
		EntityRegistry.registerModEntity(FancyEntityItem.class, "Fancy Item",
				0, this, 32, 5, true);
	}

	/**
	 * Called after Minecraft has been initialized. Used to remove recipes as
	 * necessary.
	 * 
	 * @param event
	 */
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		// Take away the crafting recipes for un-researched items if the
		// research system is enabled.
		if (!researchDisabled) {
			Crafting.handleResearchCrafting();
		}
	}

	@EventHandler
	public void onServerStarting(FMLServerStartingEvent ev) {
		proxy.onServerStarting(ev);
	}
}
