package com.defenestrationcoding.snowtech.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.defenestrationcoding.snowtech.tileentities.TileEntityReceiver;
import com.defenestrationcoding.snowtech.util.PlayerUtils;

/**
 * A block that receives and displays the haywire values fed into it.
 * 
 * @author JavaMatrix
 * 
 */
public class BlockReceiver extends BlockContainer implements
		ITileEntityProvider {

	// The tile entity bound to this block.
	TileEntityReceiver te;

	public BlockReceiver() {
		// Flammable AND inflammable!
		super(Material.wood);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		// Assign te and return it.
		return te = new TileEntityReceiver();
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer player, int metadata, float what, float these,
			float are) {
		
		if (te == null) {
			te = (TileEntityReceiver) world.getTileEntity(x, y, z);
		}

		if (PlayerUtils.hasDebugger(player)) {
			PlayerUtils.sendChat(player, "Signal: " + te.getHWSignal());
			PlayerUtils.sendChat(player, "Update Ranking: " + te.getUpdateRank());
			return true;
		}

		return false;
	}
}
