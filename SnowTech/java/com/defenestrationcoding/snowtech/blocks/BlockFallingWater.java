package com.defenestrationcoding.snowtech.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import com.defenestrationcoding.snowtech.tileentities.TileEntityFallingWater;

/**
 * A block of water that obeys gravity and physics fairly well.
 * 
 * @author JavaMatrix
 * 
 */
public class BlockFallingWater extends Block implements ITileEntityProvider {

	public BlockFallingWater(TileEntity te) {
		// Blub.
		super(Blocks.water.getMaterial());
		System.out.println(Blocks.water.getMaterial());
		setLightOpacity(0);
	}

	/**
	 * Gets a block's icon from side and meta values.
	 */
	public IIcon getIcon(int side, int meta) {
		// Just grab water's texture.
		return Blocks.water.getBlockTextureFromSide(side);
	}

	/**
	 * Prevents the player from selecting this block.
	 */
	public boolean canCollideCheck(int meta, boolean hasBoat) {
		return false;
	}

	/**
	 * Allows the block to be rendered as semi-transparent.
	 */
	public int getRenderBlockPass() {
		return 1;
	}

	/**
	 * Copied wholesale from BlockLiquid.
	 */
	public int getMixedBrightnessForBlock(IBlockAccess world, int x, int y,
			int z) {
		int l = world.getLightBrightnessForSkyBlocks(x, y, z, 0);
		int i1 = world.getLightBrightnessForSkyBlocks(x, y + 1, z, 0);
		int j1 = l & 255;
		int k1 = i1 & 255;
		int l1 = l >> 16 & 255;
		int i2 = i1 >> 16 & 255;
		return (j1 > k1 ? j1 : k1) | (l1 > i2 ? l1 : i2) << 16;
	}

	/**
	 * Cube: Yes. Opaque: No.
	 */
	public boolean isOpaqueCube() {
		return false;
	}

	/**
	 * Keeps players from colliding with the block.
	 */
	@SuppressWarnings("rawtypes")
	public void addCollisionBoxesToList(World world, int x, int y, int z,
			AxisAlignedBB bounds, List boxes, Entity who) {
		return;
	}

	/**
	 * Creates a new TileEntityFallingWater and returns it.
	 * 
	 * @param world
	 * @param meta
	 * @return A brand-new TileEntityFallingWater
	 */
	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		return new TileEntityFallingWater();
	}
}
