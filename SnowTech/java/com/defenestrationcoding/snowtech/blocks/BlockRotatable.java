package com.defenestrationcoding.snowtech.blocks;

import java.util.HashMap;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

/**
 * A parent block for anything that can be rotated by right-clicking with an
 * empty hand.
 * 
 * Seriously needs re-writing.
 * 
 * @author JavaMatrix
 * 
 */
public class BlockRotatable extends Block {

	/**
	 * 0 = Front 1 = Side
	 * 
	 * Bottom,Top,North,South,West,East
	 */
	public int[][] faceinfo = { { 0, 1, 1, 1, 1, 1 }, // Down
			{ 1, 0, 1, 1, 1, 1 }, // Up
			{ 1, 1, 0, 1, 1, 1 }, // North
			{ 1, 1, 1, 1, 1, 0 }, // East
			{ 1, 1, 1, 0, 1, 1 }, // South
			{ 1, 1, 1, 1, 0, 1 }, // West
	};

	// A helpful list of face directions in the order
	// that they are listed by meta.
	protected ForgeDirection[] facedirByMeta = { ForgeDirection.DOWN,
			ForgeDirection.UP, ForgeDirection.NORTH, ForgeDirection.EAST,
			ForgeDirection.SOUTH, ForgeDirection.WEST };

	// A helpful list of face directions in the order
	// that they are listed by Minecraft.
	protected ForgeDirection[] facedirByMC = { ForgeDirection.DOWN,
			ForgeDirection.UP, ForgeDirection.NORTH, ForgeDirection.SOUTH,
			ForgeDirection.WEST, ForgeDirection.EAST };

	// A mapping of redstone numbers to face directions.
	protected HashMap<Integer, ForgeDirection> facedirByRS = new HashMap<Integer, ForgeDirection>();

	// The current direction the block is facing.
	public ForgeDirection facing;

	// The icons (front and side).
	public IIcon[] icons = new IIcon[2];

	protected BlockRotatable(Material material) {
		super(material);
		// Populate the redstone number -> face direction map.
		facedirByRS.put(-1, ForgeDirection.UP);
		facedirByRS.put(0, ForgeDirection.NORTH);
		facedirByRS.put(1, ForgeDirection.SOUTH);
		facedirByRS.put(2, ForgeDirection.WEST);
		facedirByRS.put(3, ForgeDirection.EAST);
	}

	protected void updateFacing(IBlockAccess world, int x, int y, int z) {
		// Updates the facing variable
		facing = facedirByMC[world.getBlockMetadata(x, y, z)];
	}

	@Override
	public IIcon getIcon(int blockSide, int blockMeta) {
		// Fetch the proper icon from the table.
		return icons[faceinfo[blockMeta][blockSide]];
	}

	@Override
	public boolean rotateBlock(World worldObj, int x, int y, int z,
			ForgeDirection axis) {
		// Grab the metadata from the world object.
		int meta = worldObj.getBlockMetadata(x, y, z);
		// Cycle values 0-5
		meta = (meta + 1) % 6;
		// Update the face direction.
		facing = facedirByMC[meta];
		// Set the meta - the 3 flag forces a graphical update.
		return worldObj.setBlockMetadataWithNotify(x, y, z, meta, 3);
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer player, int side, float par7, float par8, float par9) {
		if (player.isSneaking()) {
			return false;
		}
		// Get the block from the world object.
		Block block = world.getBlock(x, y, z);
		// Rotate the block.
		block.rotateBlock(world, x, y, z, ForgeDirection.getOrientation(side));

		return true;
	}
}
