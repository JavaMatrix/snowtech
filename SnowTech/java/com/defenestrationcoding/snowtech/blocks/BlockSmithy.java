package com.defenestrationcoding.snowtech.blocks;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.particle.EffectRenderer;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import com.defenestrationcoding.snowtech.SnowTech;
import com.defenestrationcoding.snowtech.tileentities.TileEntitySmithy;
import com.defenestrationcoding.snowtech.util.PlayerUtils;
import com.defenestrationcoding.snowtech.util.Registration.SnowTechItems;

public class BlockSmithy extends Block implements ITileEntityProvider {

	public Map<Item, Float> validHammers = new HashMap<Item, Float>() {

		private static final long serialVersionUID = 5990925029039034539L;

		{
			put(SnowTechItems.hammerStone, 1.0f);
			put(SnowTechItems.hammerIron, 1.5f);
			put(SnowTechItems.hammerDiamond, 2.0f);
			put(SnowTechItems.hammerGold, 4.0f);
			put(SnowTechItems.hardenedMagicalHammer, 4.5f);
			put(SnowTechItems.hardenedMetalHammer, 2.0f);
		}
	};

	public IIcon particleIcon;

	public IIcon[] icons = new IIcon[3];

	public BlockSmithy() {
		super(Material.iron);
		setHardness(5.0f);
		setResistance(10.0F);
		setStepSound(soundTypeMetal);
	}

	@Override
	public void registerBlockIcons(IIconRegister register) {
		icons[0] = register.registerIcon("snowtech:blockSmithyTopBottom");
		icons[1] = register.registerIcon("snowtech:blockSmithySidesUnlit");
		icons[2] = register.registerIcon("snowtech:blockSmithySides");
		particleIcon = register.registerIcon("snowtech:spark");
	}

	@Override
	public IIcon getIcon(int side, int meta) {
		if (side == 0 || side == 1) {
			return icons[0];
		} else {
			return icons[meta + 1];
		}
	}

	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer player, int meta, float what, float these, float are) {
		if (player.isSneaking()) {
			return false;
		}

		TileEntitySmithy smithy = (TileEntitySmithy) world.getTileEntity(x, y,
				z);
		int metadata = world.getBlockMetadata(x, y, z);

		if (PlayerUtils.isHolding(player, Items.flint_and_steel)) {
			smithy.burnTime += 60;
			player.getHeldItem().damageItem(10, player);
			return true;
		} else if (PlayerUtils.isHolding(player,
				Item.getItemFromBlock(Blocks.iron_ore))
				&& metadata == 1) {
			if (smithy.state == 0) {
				smithy.state = 1;
				smithy.metalType = 1;
				player.getHeldItem().stackSize--;
			}

			return true;
		} else if (PlayerUtils.isHolding(player,
				Item.getItemFromBlock(Blocks.gold_ore))
				&& metadata == 1) {
			if (smithy.state == 0) {
				smithy.state = 1;
				smithy.metalType = 2;
				player.getHeldItem().stackSize--;
			}
			return true;
		} else if (PlayerUtils.isHolding(player, SnowTechItems.itemDebugger)) {
			smithy.state = 0;
			smithy.metalType = 0;
		} else if (PlayerUtils.isHolding(player, SnowTechItems.hammerAdmin)) {
			if (smithy.state > 0) {
				smithy.state--;
			}
			smithy.progress = 0;
		} else if (smithy.state == 3) {
			smithy.pop();
		}

		return false;
	}

	@Override
	public boolean addHitEffects(World world, MovingObjectPosition target,
			EffectRenderer effectRenderer) {
		return (target.sideHit == 1);
	}

	@Override
	public void onBlockClicked(World world, int x, int y, int z,
			EntityPlayer player) {
		if (player.getHeldItem() != null) {
			if (validHammers.keySet().contains(player.getHeldItem().getItem())) {
				TileEntitySmithy smithy = (TileEntitySmithy) world
						.getTileEntity(x, y, z);
				if (smithy.state > 1 && smithy.cooldown <= 0) {
					smithy.cooldown = (int) (20 / (EnchantmentHelper
							.getEnchantmentLevel(
									Enchantment.efficiency.effectId,
									player.getHeldItem()) / 2.0f));
					smithy.progress += validHammers.get(player.getHeldItem()
							.getItem())
							+ EnchantmentHelper.getEnchantmentLevel(
									Enchantment.fortune.effectId,
									player.getHeldItem());
					world.playSoundAtEntity(player, SnowTech.modid + ":forge",
							1.0f + (float) Math.random(), 1.0f);
					if (!player.capabilities.isCreativeMode) {
						player.getHeldItem().damageItem(1, player);
					}

					if (smithy.state == 2) {
						int particles = (int) (Math.random() * 15) + 10;
						for (int i = 0; i < particles; i++) {
							world.spawnParticle(
									"blockcrack_"
											+ Block.getIdFromBlock(Blocks.lava)
											+ "_0", x + 0.5
											+ (Math.random() - 0.5), y + 1, z
											+ 0.5 + (Math.random() - 0.5),
									4 * (Math.random() - 0.5),
									Math.random() * 4,
									4 * (Math.random() - 0.5));
						}
					}
				} else if (smithy.cooldown > 0) {
					System.out.println(smithy.cooldown);
					if (smithy.cooldown > 20
							&& !(EnchantmentHelper.getEnchantmentLevel(
									Enchantment.silkTouch.effectId,
									player.getHeldItem()) > 0)) {
						for (int i = 0; i < 25; i++) {
							String particleName = "";
							if (smithy.state == 2) {
								particleName = "iconcrack_"
										+ Item.getIdFromItem(SnowTechItems.moltenIngot)
										+ "_0";
							} else if (smithy.state == 3) {
								if (smithy.metalType == 1) {
									particleName = "iconcrack_"
											+ Item.getIdFromItem(SnowTechItems.hardenedMetalIngot)
											+ "_0";
								} else {
									particleName = "iconcrack_"
											+ Item.getIdFromItem(SnowTechItems.hardenedMagicalIngot)
											+ "_0";
								}
							}
							world.spawnParticle(particleName,
									x + 0.5 + (Math.random() - 0.5), y + 1, z
											+ 0.5 + (Math.random() - 0.5),
									4 * (Math.random() - 0.5),
									Math.random() * 4,
									4 * (Math.random() - 0.5));
						}
						world.playSoundAtEntity(player,
								"minecraft:random.break", 1.0f, 1.0f);
						smithy.state = 0;
						smithy.metalType = 0;
						smithy.progress = 0;
						smithy.cooldown = -5;
					}
					smithy.cooldown += 5;
				}

				world.markBlockForUpdate(x, y, z);
				smithy.markDirty();
			} else if (PlayerUtils.isHolding(player, SnowTechItems.hammerAdmin)) {
				TileEntitySmithy smithy = (TileEntitySmithy) world
						.getTileEntity(x, y, z);
				smithy.progress += 3141;
			}
		}
	}

	@Override
	public int getLightValue(IBlockAccess world, int x, int y, int z) {
		return 15 * world.getBlockMetadata(x, y, z);
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
		return new TileEntitySmithy();
	}
}
