package com.defenestrationcoding.snowtech.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import com.defenestrationcoding.snowtech.tileentities.TileEntityHaywire;
import com.defenestrationcoding.snowtech.util.PlayerUtils;

import cpw.mods.fml.client.registry.RenderingRegistry;

/**
 * A block that transmits haywire signals.
 * 
 * @author JavaMatrix
 * 
 */
public class BlockHaywire extends BlockContainer implements
		ITileEntityProvider {

	// Grab a render id for the custom render.
	public static final int RENDER_ID = RenderingRegistry
			.getNextAvailableRenderId();

	// The tile entity associated with this block.
	TileEntityHaywire te = null;

	public BlockHaywire() {
		// Baaa munch baaaaa munch.
		super(Material.grass);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		// Assign te and return it.
		return te = new TileEntityHaywire();
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer player, int metadata, float what, float these,
			float are) {
		
		if (te == null) {
			te = (TileEntityHaywire) world.getTileEntity(x, y, z);
		}

		if (PlayerUtils.hasDebugger(player)) {
			PlayerUtils.sendChat(player, "Signal: " + te.getHWSignal());
			PlayerUtils.sendChat(player, "Update Ranking: " + te.getUpdateRank());
			return true;
		}

		return false;
	}

	/**
	 * Helper function for adding boxes to the collision box list.
	 * 
	 * @param xCoord
	 *            The x position of the block.
	 * @param yCoord
	 *            The y position of the block.
	 * @param zCoord
	 *            The z position of the block.
	 * @param x1
	 *            The west limit, in pixels.
	 * @param y1
	 *            The top limit, in pixels.
	 * @param z1
	 *            The north limit, in pixels.
	 * @param x2
	 *            The east limit, in pixels
	 * @param y2
	 *            The bottom limit, in pixels.
	 * @param z2
	 *            The south limit, in pixels.
	 */
	public void setPixelBounds(int xCoord, int yCoord, int zCoord, int x1,
			int y1, int z1, int x2, int y2, int z2) {
		// Set block bounds with scaled coordinates.
		setBlockBounds(x1 / 16.0f, y1 / 16.0f, z1 / 16.0f, x2 / 16.0f,
				y2 / 16.0f, z2 / 16.0f);
	}

	@Override
	public boolean shouldSideBeRendered(IBlockAccess world, int x, int y,
			int z, int meta) {
		// Don't render normal block sides, this is
		// a custom-rendered block.
		return false;
	}

	@Override
	public boolean isOpaqueCube() {
		// It's not a cube.
		return false;
	}

	@Override
	public int getRenderType() {
		// Just grab the generated render id.
		return RENDER_ID;
	}
}
