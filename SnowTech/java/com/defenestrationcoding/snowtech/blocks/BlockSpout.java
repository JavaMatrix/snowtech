package com.defenestrationcoding.snowtech.blocks;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

import com.defenestrationcoding.snowtech.tileentities.TileEntitySpout;

public class BlockSpout extends BlockRotatable implements ITileEntityProvider {

	public int[][] faceinfo = { { 0, 2, 1, 1, 1, 1 }, // Down
			{ 2, 0, 1, 1, 1, 1 }, // Up
			{ 3, 3, 0, 2, 1, 1 }, // North
			{ 3, 3, 1, 1, 2, 0 }, // East
			{ 3, 3, 2, 0, 1, 1 }, // South
			{ 3, 3, 1, 1, 0, 2 }, // West
	};

	public IIcon[] icons = new IIcon[4];

	public BlockSpout() {
		// You lost today, sonny, but you don't have to like it!
		super(Material.rock);
	}

	@Override
	public void registerBlockIcons(IIconRegister iconLoader) {
		// Register the icons.
		icons[0] = iconLoader.registerIcon("snowtech:pumpTop");
		icons[1] = iconLoader.registerIcon("snowtech:pumpSideB");
		icons[2] = iconLoader.registerIcon("snowtech:pumpBottom");
		icons[3] = Blocks.furnace.getBlockTextureFromSide(ForgeDirection.UP
				.ordinal());
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		TileEntitySpout te = new TileEntitySpout();
		te.buildPond(world);
		return te;
	}

	@Override
	public boolean rotateBlock(World worldObj, int x, int y, int z,
			ForgeDirection axis) {
		boolean value = super.rotateBlock(worldObj, x, y, z, axis);
		TileEntitySpout te = (TileEntitySpout) worldObj.getTileEntity(x, y, z);
		te.recalcSides(worldObj);
		te.buildPond(worldObj);
		return value;
	}

	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z,
			EntityLivingBase player, ItemStack stack) {
		// Translates the player's heading to a direction that can be
		// faced towards.
		int heading = (MathHelper
				.floor_double((double) (player.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3) + 2;
		// Check if the player is looking downwards.
		if (player.rotationPitch < 30) {
			heading = 0;
		}
		// Check if the player is looking upwards
		if (player.rotationPitch > 60) {
			heading = 1;
		}

		// Set the meta to the rotation value.
		world.setBlockMetadataWithNotify(x, y, z, heading, 3);
	}

	@Override
	public IIcon getIcon(int blockSide, int blockMeta) {
		// Fetch the proper icon from the table.
		return icons[faceinfo[blockMeta][blockSide]];
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer player, int side, float par7, float par8, float par9) {
		if (!super.onBlockActivated(world, x, y, z, player, side, par7, par8,
				par9) && !world.isRemote) {
			TileEntitySpout te = (TileEntitySpout) world.getTileEntity(x, y, z);
			switch (te.problemCode) {
			case 0:
				player.addChatMessage(new ChatComponentText(
						"�2Working properly."));
				player.addChatMessage(new ChatComponentText("�2-Pond is valid."));
				player.addChatMessage(new ChatComponentText(
						"�2-Water is being supplied."));
				break;
			case 1:
				player.addChatMessage(new ChatComponentText("�4Invalid pond."));
				player.addChatMessage(new ChatComponentText(
						"�4-Pond is too big or is unbounded."));
				player.addChatMessage(new ChatComponentText(
						"�4-Check that pond's bounds are correct, with no diagonal holes."));
				player.addChatMessage(new ChatComponentText(
						"�4-Check that the pond is no more than 32 blocks across."));
				break;
			case -1:
				player.addChatMessage(new ChatComponentText(
						"�4No pond present."));
				player.addChatMessage(new ChatComponentText(
						"�4-No pond was detected in front of the spout."));
				player.addChatMessage(new ChatComponentText(
						"�4-Check that the spout's output is not blocked."));
				player.addChatMessage(new ChatComponentText(
						"�4-Check that the spout is facing the right way."));
				break;
			case 2:
				player.addChatMessage(new ChatComponentText("�6Pond is full."));
				player.addChatMessage(new ChatComponentText(
						"�6-If pond doesn't appear full, or you have changed the pond bounds, shift-right-click the spout."));
				player.addChatMessage(new ChatComponentText(
						"�6-In fact, just checking the problem probably fixed the issue."));
				break;
			case 3:
				player.addChatMessage(new ChatComponentText(
						"�4No pumped water available."));
				player.addChatMessage(new ChatComponentText(
						"�4-Use a pump to bring pumped water to the back side of the spout, the one with the round hole."));
				break;
			}
			te.recalcSides(world);
			te.buildPond(world);
		}
		return true;
	}
}
