package com.defenestrationcoding.snowtech.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import com.defenestrationcoding.snowtech.tileentities.TileEntityResearcherFred;
import com.defenestrationcoding.snowtech.util.PlayerUtils;

import cpw.mods.fml.client.registry.RenderingRegistry;

/**
 * An automatic researcher block - not currently functional.
 * 
 * @author JavaMatrix
 * 
 */
public class BlockResearcherFred extends BlockContainer implements
		ITileEntityProvider {
	// Generate a new render id to use.
	public static final int RENDER_ID = RenderingRegistry
			.getNextAvailableRenderId();

	public BlockResearcherFred() {
		// Fist of steel, head of iron!
		super(Material.iron);
	}

	@Override
	public int getRenderType() {
		// Just grab our custom render id.
		return RENDER_ID;
	}

	@Override
	public boolean shouldSideBeRendered(IBlockAccess world, int x, int y,
			int z, int meta) {
		// This is a custom-rendered block.
		return false;
	}

	@Override
	public boolean isOpaqueCube() {
		// Not a cube, or opaque.
		return false;
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		// Just make the new researcher, we don't need a reference
		// to the te.
		return new TileEntityResearcherFred();
	}

	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z,
			EntityLivingBase player, ItemStack stack) {
		// Translates the player's heading to a direction that can be
		// faced towards.
		int heading = MathHelper
				.floor_double((double) (player.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
		
		// Set the meta to the rotation value.
		world.setBlockMetadataWithNotify(x, y, z, heading, 3);
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer player, int metadata, float what, float these,
			float are) {
		// If it's the debugger, output the meta value.
		if (PlayerUtils.hasDebugger(player)) {
			player.addChatMessage(new ChatComponentText("Meta: "
					+ world.getBlockMetadata(x, y, z)));
		}

		// Didn't actually do anything.
		return false;
	}
}
