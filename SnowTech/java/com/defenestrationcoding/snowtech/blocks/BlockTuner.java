package com.defenestrationcoding.snowtech.blocks;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

import com.defenestrationcoding.snowtech.IHaywireDevice;
import com.defenestrationcoding.snowtech.tileentities.TileEntityTuner;

/**
 * A currently non-functional block that determines the resonant frequency of a block.
 * 
 * @author JavaMatrix
 *
 */
public class BlockTuner extends BlockRotatable implements IHaywireDevice, ITileEntityProvider {
	
	// The tile entity bound to this block.
	TileEntityTuner te;
	
	public BlockTuner()
	{
		// Strong as iron!
		super(Material.iron);
	}
	
	@Override
	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
	{
		// Update the face direction first.
		updateFacing(world, x, y, z);
		// Then make sure it's not the microphone side before letting
		// redstone connect.
		return facedirByRS.get(side) != facing;
	}
	
	@Override
	public void registerBlockIcons(IIconRegister iconLoader) {
		// Register the icons.
		icons[0] = iconLoader.registerIcon("snowtech:blockTunerFront");
		icons[1] = iconLoader.registerIcon("snowtech:blockTunerSide");
	}

	@Override
	public int getHWSignal() {
		// Doesn't work with a null TileEntity.
		if (te == null)
			return -1;
		
		// The TileEntity stores the signal.
		return te.signal;
	}

	@Override
	public void setHWSignal(int value) {
		// Doesn't work with a null TileEntity.
		if (te == null)
			return;
		
		// Just set the data in the TE.
		te.signal = value;
	}

	@Override
	public boolean shouldConnectOnSide(ForgeDirection side) {
		// Haywire can connect to any side except the mic.
		return side != facing;
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		// Assign the TE and then return it.
		return te = new TileEntityTuner();
	}

	@Override
	public long getUpdateRank() {
		// Always broadcasting, never receiving (not as high as a broadcaster though).
		return Long.MAX_VALUE;
	}
}
