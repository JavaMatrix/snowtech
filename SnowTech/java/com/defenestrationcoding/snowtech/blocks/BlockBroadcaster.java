package com.defenestrationcoding.snowtech.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.defenestrationcoding.snowtech.SnowTech;
import com.defenestrationcoding.snowtech.gui.GUI;
import com.defenestrationcoding.snowtech.tileentities.TileEntityBroadcaster;
import com.defenestrationcoding.snowtech.util.PlayerUtils;

/**
 * Defines a block that broadcasts a Haywire signal.
 * 
 * @author JavaMatrix
 * 
 */
public class BlockBroadcaster extends BlockContainer implements
		ITileEntityProvider {

	// The tile entity that this block is tied to.
	TileEntityBroadcaster te;

	public BlockBroadcaster() {
		// Sonic-proof!
		super(Material.wood);
		
	}

	@Override
	public TileEntity createNewTileEntity(World theWorld, int meta) {
		// Assign te and return it.
		return te = new TileEntityBroadcaster();
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer player, int metadata, float what, float these,
			float are) {

		// Make sure that the player isn't sneaking and that there is a
		// TileEntity there.
		if (player.isSneaking() || te == null)
			return false;
		
		if (PlayerUtils.hasDebugger(player)) {
			PlayerUtils.sendChat(player, "Signal: " + te.getHWSignal());
			PlayerUtils.sendChat(player, "Update Ranking: " + te.getUpdateRank());
			return true;
		}

		// Open the Broadcaster GUI.
		player.openGui(SnowTech.instance, GUI.BROADCASTER, world, x, y, z);
		return true;
	}
}
