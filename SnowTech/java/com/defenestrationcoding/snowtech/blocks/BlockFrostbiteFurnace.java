package com.defenestrationcoding.snowtech.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

import com.defenestrationcoding.snowtech.tileentities.TileEntityFrostbiteFurnace;
import com.defenestrationcoding.snowtech.util.PlayerUtils;

/**
 * A furnace that runs on cold! Concept by GoldenBuilder123. Based on furnace
 * tutorial by microjunk of Minecraft Forums.
 * 
 * @author JavaMatrix
 * 
 */
public class BlockFrostbiteFurnace extends BlockContainer {

	// The icons for each side, in the order front (on), front (off),
	// top/bottom, side.
	public IIcon[] icons = new IIcon[4];

	// The entity associated with this block.
	TileEntityFrostbiteFurnace te;

	public BlockFrostbiteFurnace() {
		// Let it go!
		super(Material.packedIce);
	}

	@Override
	public void registerBlockIcons(IIconRegister register) {
		// Pretty straight forward, innit?
		icons[0] = register
				.registerIcon("snowtech:blockFrostbiteFurnaceFrontOff");
		icons[1] = register
				.registerIcon("snowtech:blockFrostbiteFurnaceFrontOn");
		icons[2] = register.registerIcon("snowtech:blockFrostbiteFurnaceTop");
		icons[3] = register.registerIcon("snowtech:blockFrostbiteFurnaceSide");
	}

	@Override
	public IIcon getIcon(int side, int meta) {

		if (te == null) {
			// Different icons if it's in the player's inventory.
			return getInvIcon(side, meta);
		}

		// Choose the right icon based on the given side.
		if (side == te.front.ordinal()) {
			if (te.isBurning()) {
				return icons[1];
			} else {
				return icons[0];
			}
		} else if (side == ForgeDirection.UP.ordinal()) {
			return icons[2];
		}

		return icons[3];
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, Block me, int meta) {
		// Get the tile entity at this position.
		TileEntityFrostbiteFurnace teff = (TileEntityFrostbiteFurnace) world
				.getTileEntity(x, y, z);

		// Iterate through it's inventory, throwing out anything
		// therein.
		for (int i = 0; i < teff.getSizeInventory(); i++) {
			// Grab the items.
			ItemStack items = teff.getStackInSlot(i);

			if (items != null) {
				// Throw the items randomly! WHEEE!
				Random r = new Random();
				float dx = r.nextFloat() * 0.8F + 0.1F;
				float dy = r.nextFloat() * 0.8F + 0.1F;
				float dz = r.nextFloat() * 0.8F + 0.1F;

				while (items.stackSize > 0) {
					int num = r.nextInt(21) + 10;
					num = Math.max(num, items.stackSize);
					items.stackSize -= num;

					ItemStack toThrow = items.copy();
					toThrow.stackSize = num;

					EntityItem entityItem = new EntityItem(world, x + dx, y
							+ dy, z + dz, toThrow);

					entityItem.motionX = (double) ((float) r.nextGaussian() * 0.05);
					entityItem.motionY = (double) ((float) r.nextGaussian() * 0.05 + 0.2F);
					entityItem.motionZ = (double) ((float) r.nextGaussian() * 0.05);

					world.spawnEntityInWorld(entityItem);
				}
			}
		}
	}

	private IIcon getInvIcon(int side, int meta) {
		// We'll say that South is the front, eh?
		if (side == ForgeDirection.SOUTH.ordinal()) {
			return icons[0];
		} else if (side == ForgeDirection.UP.ordinal()) {
			return icons[2];
		}

		return icons[3];
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer player, int side, float par7, float par8, float par9) {
		// If the player is using the debug tool,
		if (PlayerUtils.hasDebugger(player)) {
			// Spew out a whole bunch of debug stuff.
			player.addChatMessage(new ChatComponentText(te.getPercentDone()
					+ "% Complete"));
			player.addChatMessage(new ChatComponentText(te.snowmen
					+ " snowmen found."));
			player.addChatMessage(new ChatComponentText("Burn Time: "
					+ te.furnaceBurnTime));
			player.addChatMessage(new ChatComponentText("Cook Time: "
					+ te.furnaceCookTime));
			player.addChatMessage(new ChatComponentText(
					"Current Item Burn Time: " + te.currentItemBurnTime));
			player.addChatMessage(new ChatComponentText("Smelt Time: "
					+ te.smeltTime));
			return false;
		}

		// Open the gui, provided the player isn't sneaking.
		if (!player.isSneaking()) {
			//TODO
			player.addChatMessage(new ChatComponentText("This block is WIP and currently doesn't do anything."));
			
//			te = (TileEntityFrostbiteFurnace) world.getTileEntity(x, y, z);
//			player.openGui(SnowTech.instance, GUI.FROSTBITE_FURNACE, world, x,
//					y, z);
//			return true;
		}

		return false;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		// Assign te and return it.
		return (te = new TileEntityFrostbiteFurnace());
	}

}
