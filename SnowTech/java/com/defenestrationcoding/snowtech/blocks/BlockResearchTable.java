package com.defenestrationcoding.snowtech.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import com.defenestrationcoding.snowtech.SnowTech;
import com.defenestrationcoding.snowtech.gui.GUI;
import com.defenestrationcoding.snowtech.tileentities.TileEntityResearchTable;

import cpw.mods.fml.client.registry.RenderingRegistry;

/**
 * A block that allows the player to conduct research.
 * 
 * @author JavaMatrix
 * 
 */
public class BlockResearchTable extends BlockContainer implements
		ITileEntityProvider {

	// Generate a new render id automatically.
	public static final int RENDER_ID = RenderingRegistry
			.getNextAvailableRenderId();

	public BlockResearchTable() {
		// Woody, but no buzz.
		super(Material.wood);
	}

	@Override
	public int getRenderType() {
		// Just grab out custom-generated render id.
		return RENDER_ID;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer player, int metadata, float what, float these,
			float are) {
		// Grab the TileEntity at this position.
		TileEntity te = world.getTileEntity(x, y, z);

		// Don't open up the gui if the player is sneaking or there's no
		// tile entity.
		if (player.isSneaking() || te == null)
			return false;

		// Open up the research GUI.
		player.openGui(SnowTech.instance, GUI.RESEARCH_TABLE, world, x, y, z);
		return true;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		// No need to store the TE.
		return new TileEntityResearchTable();
	}

	@Override
	public boolean shouldSideBeRendered(IBlockAccess world, int x, int y,
			int z, int meta) {
		// Don't render sides, this is a custom-rendered block.
		return false;
	}

	@Override
	public boolean isOpaqueCube() {
		// Not a cube.
		return false;
	}
}
