package com.defenestrationcoding.snowtech.blocks;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

import com.defenestrationcoding.snowtech.tileentities.TileEntityFallingWater;
import com.defenestrationcoding.snowtech.util.BlockPosition;
import com.defenestrationcoding.snowtech.util.Registration.SnowTechBlocks;

public class BlockPump extends Block {

	// The icons for each side, in the order top, bottom, side a, all other
	// sides
	public IIcon[] icons = new IIcon[4];

	public BlockPump() {
		super(Material.rock);
	}

	@Override
	public void registerBlockIcons(IIconRegister register) {
		
		icons[0] = register.registerIcon("snowtech:pumpTop");
		icons[1] = register.registerIcon("snowtech:pumpBottom");
		icons[2] = register.registerIcon("snowtech:pumpSideA");
		icons[3] = register.registerIcon("snowtech:pumpSideB");
	}

	@Override
	public IIcon getIcon(int side, int meta) {
		if (side == ForgeDirection.UP.ordinal()) {
			return icons[0];
		} else if (side == ForgeDirection.DOWN.ordinal()) {
			return icons[1];
		} else if (side == ForgeDirection.SOUTH.ordinal()) {
			return icons[2];
		}
		return icons[3];
	}

	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer player, int meta, float what, float these, float are) {

		// Don't operate when the player is sneaking.
		if (player.isSneaking()) {
			return false;
		}

		// Build a list of neighboring blocks.
		List<BlockPosition> inputs = new ArrayList<BlockPosition>();
		inputs.add(new BlockPosition(x + 1, y, z));
		inputs.add(new BlockPosition(x - 1, y, z));
		inputs.add(new BlockPosition(x, y, z + 1));
		inputs.add(new BlockPosition(x, y, z - 1));
		inputs.add(new BlockPosition(x, y - 1, z));

		BlockPosition top = new BlockPosition(x, y + 1, z);

		for (BlockPosition position : inputs) {
			if (position.getBlock(world).equals(Blocks.water)) {
				// Suck up the water.
				position.setBlock(world, Blocks.air);
				// And now we need to put water above this block.
				// First, let's see if there's already a block there.
				if (top.getBlock(world).equals(Blocks.air)
						|| top.getBlock(world).equals(
								SnowTechBlocks.blockFallingWater)) {
					// Now check whether it's air or falling water.
					if (top.getBlock(world).equals(Blocks.air)) {
						// Just put a chunk of water on top.
						top.setBlock(world, SnowTechBlocks.blockFallingWater);
						System.out.println("Put water.");
						return true;
					} else {
						// Must be falling water.
						// Just add one pressure.
						TileEntityFallingWater te = (TileEntityFallingWater) top
								.getTE(world);
						te.pressure++;
						System.out.println("New pressure: " + te.pressure);
						return true;
					}
				} else {
					// Tell the player we can't continue.
					player.addChatMessage(new ChatComponentText(
							"Can't pump water, the top is blocked!"));
					return true;
				}
			}
		}

		return false;
	}
}
