package com.defenestrationcoding.snowtech.renderers;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;

import org.lwjgl.opengl.GL11;

import com.defenestrationcoding.snowtech.model.ModelResearchTable;

public class RendererResearchTableItem implements IItemRenderer {
	ResourceLocation texture = new ResourceLocation("snowtech",
			"textures/blocks/blockResearchTableTexture.png");
	ModelResearchTable model = new ModelResearchTable();

	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
			ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		GL11.glPushMatrix();
		switch (type) {
		case EQUIPPED:
		case EQUIPPED_FIRST_PERSON:
			GL11.glTranslated(0.0, 1, 0.0);
			break;
		default:
			GL11.glTranslated(0.0, 1, 0.0);
			break;
		}
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
		GL11.glPushMatrix();
		GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
		this.model
				.render((Entity) null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
		GL11.glPopMatrix();
		GL11.glPopMatrix();
	}
}
