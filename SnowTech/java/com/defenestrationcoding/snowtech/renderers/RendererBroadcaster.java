package com.defenestrationcoding.snowtech.renderers;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;

import org.lwjgl.opengl.GL11;

import com.defenestrationcoding.snowtech.tileentities.TileEntityBroadcaster;

public class RendererBroadcaster extends TileEntitySpecialRenderer {

	@Override
	public void renderTileEntityAt(TileEntity te, double x, double y,
			double z, float f) {
		
		if (!te.getWorldObj().getBlock((int) te.xCoord, (int) te.yCoord + 1, (int) te.zCoord).equals(Blocks.air)) {
			return;
		}
		// Some magic values of some kind.
		float randomNumber1 = 1.6F;
		float randomNumber2 = randomNumber1 / 60;
		
		// We'll need this render manager.
		RenderManager rm = RenderManager.instance;
		
		// And this font renderer.
		FontRenderer fr = rm.getFontRenderer();
		
		// Now some transforms.
		GL11.glPushMatrix();
		// Vary the offsets as needed.
		GL11.glTranslatef((float) x + 0.5f, (float) y + 1.5f, (float) z + 0.5f);
		GL11.glNormal3f(0.0f, 1.0f, 0.0f);
		// Y Axis rotation
		GL11.glRotatef(-rm.playerViewY, 0.0f, 1.0f, 0.0f);
		// X Axis rotation
		GL11.glRotatef(rm.playerViewX, 1.0f, 0.0f, 0.0f);
		// Scale by that random value.
		GL11.glScalef(-randomNumber2, -randomNumber2, -randomNumber2);
		
		// Find the length of the string we want to draw.
		String text = ((TileEntityBroadcaster) te).signal + " Notches";
		int textWidth = fr.getStringWidth(text);
		
		GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		GL11.glDisable(GL11.GL_LIGHTING);
		fr.drawString(text, -textWidth / 2, 0, 0x404040);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glPopMatrix();
	}
}
