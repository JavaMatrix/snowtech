package com.defenestrationcoding.snowtech.renderers;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import com.defenestrationcoding.snowtech.model.ModelResearcherFred;

public class RendererResearcherFred extends TileEntitySpecialRenderer {

	ResourceLocation texture = new ResourceLocation("snowtech",
			"textures/blocks/blockResearcherFredTexture.png");
	ModelResearcherFred model = new ModelResearcherFred();

	@Override
	public void renderTileEntityAt(TileEntity te, double x, double y, double z,
			float f) {
		GL11.glPushMatrix();
		GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F);
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
		GL11.glPushMatrix();
		
		int meta = te.getWorldObj().getBlockMetadata(te.xCoord, te.yCoord, te.zCoord);
		
		GL11.glRotatef(90.0F * (3 - meta), 0, 1, 0);
		GL11.glRotatef(180F,0.0F, 0.0F, 1.0F);
		this.model
				.render((Entity) null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
		GL11.glPopMatrix();
		GL11.glPopMatrix();
	}

}
