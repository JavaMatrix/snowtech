package com.defenestrationcoding.snowtech.fx;

import net.minecraft.client.particle.EntityCritFX;
import net.minecraft.world.World;

public class EntityCustomFX extends EntityCritFX {
	
	public EntityCustomFX(World world, double x,
			double y, double z, double vx, double vy, double vz) {
		super(world, x, y, z, vx, vy, vz);
		this.setRBGColorF(1.0f, 0.67f, 0.0f);
		this.onUpdate();
	}
}
