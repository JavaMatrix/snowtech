package com.defenestrationcoding.snowtech.commands;

import com.defenestrationcoding.snowtech.tileentities.TileEntityFallingWater;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;

public class CommandKillWater extends CommandBase {

	public static int waterMaxAge = 0;
	
	@Override
	public String getCommandName() {
		return "togglephysicswater";
	}

	@Override
	public String getCommandUsage(ICommandSender p_71518_1_) {
		return "/togglephysicswater";
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		if (TileEntityFallingWater.maxAge > 0) {
		waterMaxAge = TileEntityFallingWater.maxAge;
		TileEntityFallingWater.maxAge = 0;
		} else {
			TileEntityFallingWater.maxAge = waterMaxAge;
		}
	}
}
