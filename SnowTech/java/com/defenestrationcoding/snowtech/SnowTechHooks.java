package com.defenestrationcoding.snowtech;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockObsidian;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.ChatComponentText;
import net.minecraftforge.event.entity.living.LivingSetAttackTargetEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;

import com.defenestrationcoding.snowtech.research.Research;
import com.defenestrationcoding.snowtech.research.ResearchItem;
import com.defenestrationcoding.snowtech.util.Registration.SnowTechBlocks;
import com.defenestrationcoding.snowtech.util.Registration.SnowTechItems;

import cpw.mods.fml.common.eventhandler.Event.Result;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

/**
 * Listens for various events within Minecraft and implements custom behaviors
 * when they happen.
 * 
 * @author JavaMatrix
 * 
 */
public class SnowTechHooks {

	// A list of things that should not spawn.
	public static List<Class<? extends EntityLivingBase>> deniedSpawns = new ArrayList<Class<? extends EntityLivingBase>>();

	/**
	 * Listens for a player lighting something on fire and tries to prevent
	 * Nether portals from lighting.
	 * 
	 * @param e
	 *            The event being handled.
	 */
	@SubscribeEvent
	public void fireLit(PlayerInteractEvent e) {
		// Verify that we can use the block, that the player is holding an item,
		// that that item is Flint and Steel, and that the player is
		// right-clicking.
		if (e.useBlock == Result.DENY) {
			return;
		}
		if (e.entityPlayer.getHeldItem() == null) {
			return;
		}
		if (!e.entityPlayer.getHeldItem().getItem()
				.equals((Items.flint_and_steel))) {
			return;
		}
		if (e.action != Action.RIGHT_CLICK_BLOCK) {
			return;
		}

		// Grab the research that allows Nether portals to be created.
		ResearchItem quaererology = Research.researchByName.get("Quaererology");

		// Make sure that the research exists and that it's not complete
		// before stopping the player from making a portal.
		if (quaererology == null || quaererology.isComplete()) {
			return;
		}

		// Make sure that the block at the given position is Obsidian.
		Block b = Minecraft.getMinecraft().theWorld.getBlock(e.x, e.y, e.z);
		if (b instanceof BlockObsidian && e.isCancelable()) {
			// Cancel the event and tell the player why.
			e.setCanceled(true);
			e.entityPlayer.addChatMessage(new ChatComponentText(
					"You must research Quaererology to do this."));
		}
	}

	/**
	 * Tries to keep denied spawns from spawning. Currently not working well.
	 * 
	 * @param e
	 *            The event being handled.
	 */
	@SubscribeEvent
	public void entitySpawnCheck(LivingSpawnEvent e) {
		// Check if the entity should be denied from spawning.
		if (deniedSpawns.contains(e.entityLiving.getClass())) {
			e.setResult(Result.DENY);
			if (e.isCancelable()) {
				e.setCanceled(true);
			}
		}
	}

	/**
	 * Makes sure that the player has researched taming before allowing them to
	 * tame animals.
	 * 
	 * @param e
	 *            The event being handled.
	 */
	@SubscribeEvent
	public void animalTamedEvent(LivingSetAttackTargetEvent e) {
		// Make sure that the entity in question is tameable.
		if (!(e.entity instanceof EntityTameable))
			return;

		// Make sure that the research exists and isn't complete.
		if (Research.researchByName.get("Beastmastering") == null
				|| Research.researchByName.get("Beastmastering").isComplete())
			return;

		// Untame the beast and tell the owner why.
		if (((EntityTameable) e.entity).getOwner() != null) {
			EntityTameable pet = (EntityTameable) e.entity;
			EntityPlayer owner = (EntityPlayer) pet.getOwner();
			owner.addChatMessage(new ChatComponentText(
					"Only Beastmasters have the skill to tame creatures."));
		}
		((EntityTameable) e.entity).setTamed(false);
	}

	/**
	 * Used to cancel "whack" events while holding the Sword of Mendragor.
	 * 
	 * @param e
	 *            The event being handled.
	 */
	@SubscribeEvent
	public void whackEvent(PlayerInteractEvent e) {
		if (e.entityPlayer.getHeldItem() != null) {
			if (e.entityPlayer.getHeldItem().getItem()
					.equals(SnowTechItems.swordOfMendragor)) {
				if (e.action == Action.LEFT_CLICK_BLOCK) {
					if (e.isCancelable()) {
						e.setCanceled(true);
					}
				}
			}
		}
	}

	/**
	 * Called when a tooltip for an item is created.
	 * 
	 * @param ev
	 *            The event related to the tooltip.
	 */
	@SubscribeEvent
	public void onTooltip(ItemTooltipEvent ev) {
		if (ev.itemStack.getItem().equals(SnowTechItems.enthalpite)) {
			ev.toolTip.add("How can a crystal be sinister? And");
			ev.toolTip.add("yet, this one is.");
		} else if (ev.itemStack.getItem().equals(SnowTechItems.enthalpiteShard)) {
			ev.toolTip.add("Only a tiny bit evil, but it emanates");
			ev.toolTip.add("a desire to be united with others like");
			ev.toolTip.add("it. But is that really a good idea?");
		} else if (ev.itemStack.getItem().equals(SnowTechItems.gear)) {
			ev.toolTip.add("If you don't know what a gear is,");
			ev.toolTip.add("crawl out from under that rock.");
		} else if (ev.itemStack.getItem().equals(SnowTechItems.itemDebugger)) {
			ev.toolTip.add("Don't look directly at the bugs!");
		} else if (ev.itemStack.getItem().equals(
				SnowTechItems.itemTelepathicSnow)) {
			ev.toolTip.add("This snow is new.  Possibly alien.");
		} else if (ev.itemStack.getItem().equals(
				Item.getItemFromBlock(SnowTechBlocks.blockBroadcaster))) {
			ev.toolTip.add("Broadcasts a player-set");
			ev.toolTip.add("Haywire signal to any neighboring");
			ev.toolTip.add("blocks that can receive them.");
		} else if (ev.itemStack.getItem().equals(
				Item.getItemFromBlock(SnowTechBlocks.blockPump))) {
			ev.toolTip.add("This ingenious design will allow you");
			ev.toolTip.add("to pump water! Just put it above");
			ev.toolTip.add("a lake and right click, and it will");
			ev.toolTip.add("create pressurized water above itself.");
			ev.toolTip.add("A Spout on the other end will return the");
			ev.toolTip.add("water to normal.");
		} else if (ev.itemStack.getItem().equals(Item.getItemFromBlock(SnowTechBlocks.blockSpout))) {
			ev.toolTip.add("Use a Pump to get falling water to the");
			ev.toolTip.add("side with the round hole in it, and face");
			ev.toolTip.add("the square side towards an empty or partially");
			ev.toolTip.add("empty pond, and this will fill it up for you");
			ev.toolTip.add("automagically.");
		} else if (ev.itemStack.getItem().equals(
				Item.getItemFromBlock(SnowTechBlocks.blockResearcherFred))) {
			ev.toolTip.add("�b\"Hi! My name is Fred. I'm not too bright.\"");
			ev.toolTip.add("(Hook him to a Research Table through a Psychic");
			ev.toolTip.add("Tube, and he'll research things for you.)");
		} else if (ev.itemStack.getItem().equals(
				Item.getItemFromBlock(SnowTechBlocks.blockHaywire))) {
			ev.toolTip.add("This fibrous cable can transfer signals");
			ev.toolTip.add("in the form of numbers, in the range 0-255.");
		} else if (ev.itemStack.getItem().equals(
				Item.getItemFromBlock(SnowTechBlocks.blockResearchTable))) {
			ev.toolTip.add("You'll need to research things here at");
			ev.toolTip.add("the cost of experience before you craft");
			ev.toolTip.add("them.");
		} else if (ev.itemStack.getItem().equals(
				Item.getItemFromBlock(SnowTechBlocks.blockFrostbiteFurnace))) {
			ev.toolTip.add("This furnace seems to run on cold. If you");
			ev.toolTip.add(" could find a cold place, it might even");
			ev.toolTip.add("run without fuel...");
		} else if (ev.itemStack.getItem().equals(
				Item.getItemFromBlock(SnowTechBlocks.blockReceiver))) {
			ev.toolTip.add("This handy block will accept haywire");
			ev.toolTip.add("signals and display them above itself");
			ev.toolTip.add("with a nifty holo-display.");
		} else if (ev.itemStack.getItem().equals(
				Item.getItemFromBlock(SnowTechBlocks.blockTuner))) {
			ev.toolTip.add("Point this at a block and power it");
			ev.toolTip.add("with redstone, and it will send out");
			ev.toolTip.add("a haywire signal equal to the resonant");
			ev.toolTip.add("frequency of the block in front of it.");
		}
	}
	
}
