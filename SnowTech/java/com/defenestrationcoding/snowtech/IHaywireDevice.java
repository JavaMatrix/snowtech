package com.defenestrationcoding.snowtech;

import net.minecraftforge.common.util.ForgeDirection;

/**
 * An interface defining the functions needed to make a
 * Haywire device.
 * 
 * @author JavaMatrix
 *
 */
public interface IHaywireDevice {
	/**
	 * Used in haywire updates to acquire the signal being broadcast from this
	 * device.
	 * 
	 * @return The current signal being broadcast.
	 */
	public int getHWSignal();
	
	/**
	 * Used in haywire updates to change the value being carried by this device.
	 * @param value The new value to store.
	 */
	public void setHWSignal(int value);
	
	/**
	 * Used in haywire updates to determine signal precedence -  a higher update number
	 * means higher precedence.  It is recommended to use the current system time of the
	 * last signal change.
	 * 
	 * @return The update rank of this device.
	 */
	public long getUpdateRank();
	
	/**
	 * Used by haywires to determine whether to connect to a block.
	 * 
	 * @param side
	 *            The side in question.
	 * @return Whether the haywire is allowed to connect to this side.
	 */
	public boolean shouldConnectOnSide(ForgeDirection side);
}
