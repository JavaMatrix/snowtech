package com.defenestrationcoding.snowtech.research;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;

import com.defenestrationcoding.snowtech.SnowTechHooks;

public class SpecialResearchHandler {
	
	public static void handleResearched(String type) {
		
	}
	
	@SuppressWarnings("unchecked")
	public static void handleRemoval(String type) {
		String[] args = type.split(" ");
		
		switch (args[0]) {
			case "portal":
				// Handled in SnowTechHooks.java
				break;
			case "enchantment":
				int enchantment = Integer.parseInt(args[1]);
				try {
					Enchantment.enchantmentsList[enchantment] = null;
					Enchantment.enchantmentsBookList[enchantment] = null;
				} catch (ArrayIndexOutOfBoundsException e) {
					System.out.println("Error 1337: Unknown enchantment #" + enchantment + ".");
				}
				break;
			case "entity":
				SnowTechHooks.deniedSpawns.add((Class<? extends EntityLivingBase>) EntityList.stringToClassMapping.get(args[1]));
		}
	}
}
