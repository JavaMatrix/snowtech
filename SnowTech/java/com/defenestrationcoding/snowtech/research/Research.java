package com.defenestrationcoding.snowtech.research;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Research {

	public static List<Item> tech = new ArrayList<Item>();
	public static List<ResearchItem> dependencies = new ArrayList<ResearchItem>();
	public static List<ResearchItem> researches = new ArrayList<ResearchItem>();
	public static int tier = 0;
	public static String name = "Research";
	public static String desc = "It's some generic research.  Hooray.";

	public static Map<String, ResearchItem> researchByName = new HashMap<String, ResearchItem>();
	private static List<String> specials = new ArrayList<String>();

	public static List<ResearchItem> getAllResearch() {
		return researches;
	}

	protected static void addResearch(List<ItemStack> tech,
			List<ResearchItem> dependencies, int tier, String name,
			String desc, int iqsecs) {
		researches.add(new ResearchItem(tech, dependencies, tier, name, desc,
				iqsecs));
	}

	public static Item itemize(Block b) {
		return Item.getItemFromBlock(b);
	}

	public static void forciblyUnlock(ResearchItem r) {
		// Make sure we have all the dependencies unlocked.
		if (!r.areDependenciesComplete()) {
			// If not, unlock them, recursion-style.
			for (ResearchItem d : r.dependencies) {
				forciblyUnlock(d);
			}
		}

		// And then just run the research's complete function.
		r.complete();
	}

	protected static List<ItemStack> parseItems(Node n) {
		NodeList techs = n.getChildNodes();
		List<ItemStack> retval = new ArrayList<ItemStack>();

		for (int i = 0; i < techs.getLength(); i++) {
			String name = techs.item(i).getTextContent();
			String damage = "";
			String special = "false";
			
			try {
				damage = techs.item(i).getAttributes().getNamedItem("damage").getTextContent();
			} catch (NullPointerException npe) {
				damage = "";
			}
			
			try {
				special = techs.item(i).getAttributes().getNamedItem("special").getTextContent();
			} catch (NullPointerException npe) {
				special = "false";
			}
			
			if (special != "false") {
				SpecialResearchHandler.handleRemoval(name);
				specials.add(name);
			}
			
			if (damage == "") {
				ItemStack t = new ItemStack(
						(Item) Item.itemRegistry.getObject(name), 0);
				if (t != null && t.getItem() != null) {
					retval.add(t);
				} else if (name.matches(".*\\w.*")) System.out.println("Couldn't parse item: " + name);
			}
			else if (damage.trim().equals("all")) {
				Item item = (Item) Item.itemRegistry.getObject(name);
				for (int x = 0; i < item.getMaxDamage(); i++) {
					ItemStack t = new ItemStack(
							item, 0, x);
					if (t != null && t.getItem() != null) {
						retval.add(t);
					} else if (name.matches(".*\\w.*")) System.out.println("Couldn't parse item: " + name);
				}
			}
			else if (damage.indexOf('-') == -1) {
				System.out.println(damage);
				int damageNum = Integer.parseInt(damage);
				ItemStack t = new ItemStack(
						(Item) Item.itemRegistry.getObject(name), 0, damageNum);
				if (t != null && t.getItem() != null) {
					retval.add(t);
				} else if (name.matches(".*\\w.*")) System.out.println("Couldn't parse item: " + name);
			}
			else {
				String[] pieces = damage.split("-");
				int start = Integer.parseInt(pieces[0]);
				int end = Integer.parseInt(pieces[1]);
				Item item = (Item) Item.itemRegistry.getObject(name);
				
				for (int x = start; i <= end; i++) {
					ItemStack t = new ItemStack(
							item, 0, x);
					if (t != null && t.getItem() != null) {
						retval.add(t);
					} else if (name.matches(".*\\w.*")) System.out.println("Couldn't parse item: " + name);
				}
			}
		}

		return retval;
	}

	public static void loadFromXML(Document d) {
		
		NodeList research = d.getElementsByTagName("item");

		for (int i = 0; i < research.getLength(); i++) {
			Node researchNode = research.item(i);

			int tier = Integer.parseInt(((Element) researchNode)
					.getAttribute("tier"));

			String name = "ERROR NO NAME", desc = "ERROR NO DESCRIPTION";
			List<ItemStack> tech = new ArrayList<ItemStack>();
			List<ResearchItem> parents = new ArrayList<ResearchItem>();
			specials.clear();
			int iqsecs = 0;

			NodeList children = researchNode.getChildNodes();
			for (int j = 0; j < children.getLength(); j++) {
				Node info = children.item(j);

				switch (info.getNodeName().toLowerCase()) {
				case "name":

					name = info.getTextContent();
					break;
				case "desc":
					desc = info.getTextContent();
					break;
				case "technologies":
					tech.addAll(parseItems(info));
					break;
				case "parents":
					NodeList parentList = info.getChildNodes();
					for (int k = 0; k < parentList.getLength(); k++) {
						String parentName = parentList.item(k).getTextContent();
						if (researchByName.containsKey(parentName)) {
							parents.add(researchByName.get(parentName));
						} else {
							System.out.println("Unknown research: "
									+ parentName);
						}
					}
					break;
				case "iqsecs":
					iqsecs = Integer.parseInt(info.getTextContent());
					break;
				case "#text": case "#comment":
					break;
				default:
					System.out.println("Error Research-Load-0: Can't parse node "
							+ info.getNodeName());
				}
			}
			ResearchItem r = new ResearchItem(tech, parents, tier, name, desc,
					iqsecs);
			r.specials = Research.specials;

			if (r.tier == 0) {
				r.complete();

			}

			researches.add(r);
			researchByName.put(name, r);
		}
	}

	public static void load(String path) throws ParserConfigurationException,
			IOException, SAXException {
		File f = new File(path);

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

		Document d;

		if (f.exists()) {
			d = dBuilder.parse(f);
		} else {
			d = dBuilder.parse(Minecraft
					.getMinecraft()
					.getResourceManager()
					.getResource(
							new ResourceLocation("snowtech", "text/tree.xml"))
					.getInputStream());
		}

		loadFromXML(d);

		System.out.println(f.getAbsolutePath());
	}

	public static List<ResearchItem> getCurrentResearch() {
		List<ResearchItem> current = new ArrayList<ResearchItem>();

		for (ResearchItem r : researches) {
			if (r.areDependenciesComplete() && !r.isComplete()) {
				current.add(r);
			}
		}

		return current;
	}
}
