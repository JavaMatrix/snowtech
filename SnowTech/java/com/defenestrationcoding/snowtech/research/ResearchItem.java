package com.defenestrationcoding.snowtech.research;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;

@SuppressWarnings("unchecked")
public class ResearchItem {
	protected boolean complete = false;
	public List<ItemStack> items = new ArrayList<ItemStack>();
	public List<ResearchItem> dependencies = new ArrayList<ResearchItem>();
	public List<IRecipe> recipes = new ArrayList<IRecipe>();
	public List<String> specials = new ArrayList<String>();
	public Map<ItemStack, ItemStack> smelts = new HashMap<ItemStack, ItemStack>();
	public int tier = 0;
	public String name = "Research";
	public String desc = "It's some generic research.  Hooray.";
	public int iqsecs = 0;
	
	public ResearchItem(List<ItemStack> tech, List<ResearchItem> dependencies, int tier, String name, String desc, int iqsecs) {
		// Set the variables - this is a really basic constructor.
		items = tech;
		this.dependencies = dependencies;
		this.tier = tier;
		this.name = name;
		this.desc = desc;
		this.iqsecs = iqsecs;
	}
	
	public boolean complete() {
		// Check those dependencies, cap'n.
		if (!areDependenciesComplete()) return false;
		
		// Turn on the complete flag
		complete = true;
		
		// Make the new items craft-able.
		// - Get the list of recipes.
		List<IRecipe> cookbook = CraftingManager.getInstance().getRecipeList();
		
		// - Loop through the recipes associated with this research.
		for (IRecipe r : recipes) {
			// -- Check that the recipe isn't already registered, then register it.
			if (!cookbook.contains(r)) {
				cookbook.add(r);
			}
		}
		
		// - Loop through smelts associated with this research.
		for (ItemStack key : smelts.keySet()) {
			// -- Check that the recipe isn't already registered, then register it.
			if (!FurnaceRecipes.smelting().getSmeltingList().containsKey(key)) {
				FurnaceRecipes.smelting().getSmeltingList().put(key, smelts.get(key));
			}
		}
		
		// - And handle special research
		for (String special : specials) {
			SpecialResearchHandler.handleResearched(special);
		}
		
		return true;
	}
	
	public boolean isComplete() {
		return complete && areDependenciesComplete();
	}
	
	public boolean areDependenciesComplete() {
		boolean b = true;
		
		for (ResearchItem r : dependencies) {
			if (!r.isComplete()) {
				b = false;
			}
		}
		
		return b;
	}
}
