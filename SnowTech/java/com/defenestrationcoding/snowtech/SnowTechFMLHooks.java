package com.defenestrationcoding.snowtech;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import com.defenestrationcoding.snowtech.items.IHardenedItem;
import com.defenestrationcoding.snowtech.util.Registration.SnowTechItems;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;

public class SnowTechFMLHooks {

	@SubscribeEvent
	public void onItemCrafted(PlayerEvent.ItemCraftedEvent ev) {
		if (ev.crafting.getItem() instanceof IHardenedItem) {
			System.out.println("Crafting handler caught a "
					+ ev.crafting.getItem().getClass() + " event.");
			int durability = 0;
			for (int i = 0; i < ev.craftMatrix.getSizeInventory(); i++) {
				if (ev.craftMatrix.getStackInSlot(i) != null) {
					ItemStack slot = ev.craftMatrix.getStackInSlot(i);
					int hardness = slot.getItemDamage();
					slot.setItemDamage(0);
					if (slot.getItem().equals(SnowTechItems.hardenedMetalIngot)) {
						durability += hardness;
					}
				}
			}
			// ev.crafting.setItemDamage(durability);
			NBTTagCompound nbt = new NBTTagCompound();
			if (ev.crafting.hasTagCompound()) {
				nbt = ev.crafting.getTagCompound();
			}
			nbt.setInteger("maxDurability", durability);
			ev.crafting.setTagCompound(nbt);
		}
	}
}
