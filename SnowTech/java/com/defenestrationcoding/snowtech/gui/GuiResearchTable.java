package com.defenestrationcoding.snowtech.gui;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import com.defenestrationcoding.snowtech.SnowTech;
import com.defenestrationcoding.snowtech.containers.ContainerResearchTable;
import com.defenestrationcoding.snowtech.research.Research;
import com.defenestrationcoding.snowtech.research.ResearchItem;
import com.defenestrationcoding.snowtech.tileentities.TileEntityResearchTable;

/**
 * The GUI for the Research Table.
 * 
 * @author JavaMatrix
 * 
 */
public class GuiResearchTable extends GuiContainer {
	// The background texture location.
	ResourceLocation texture = new ResourceLocation(SnowTech.modid,
			"textures/gui/researchtable.png");
	// Remembers which button is selected.
	ToggleButton selectedButton;

	// The GUI top and left coordinates.
	int guiTop = 0;
	int guiLeft = 0;

	// The available research buttons.
	List<ToggleButton> button = new ArrayList<ToggleButton>();
	// Default text for when no research is selected.
	String title = "No Research Selected";
	String body = "Click one of the available research items to begin!";
	// The Research button.
	GuiButton researchIt;
	// The scroll position.
	int scrollPos = 0;

	// The scrolly buttons! CLICKY CLICKY SCROLL FUN.
	GuiButton up;
	GuiButton down;

	/**
	 * The constructor
	 * 
	 * @param invPlayer
	 *            The player's inventory.
	 * @param entity
	 *            The TE to bind to.
	 */
	public GuiResearchTable(InventoryPlayer invPlayer,
			TileEntityResearchTable entity) {

		// SUPER CONSTRUCTOR!
		super(new ContainerResearchTable());

		// Set the width and height.
		xSize = 252;
		ySize = 166;
	}

	@SuppressWarnings("unchecked")
	public void initGui() {
		// Calculate the left and top coordinates.
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;

		// Initialize the buttons.
		researchIt = new GuiButton(9, guiLeft + 27, guiTop + ySize - 20 - 8,
				65, 20, "Research");
		researchIt.enabled = false;
		buttonList.add(researchIt);

		buttonList.add(down = new GuiButton(100, guiLeft + 228, guiTop + 138,
				20, 20, "-"));
		buttonList.add(up = new GuiButton(-100, guiLeft + 228, guiTop + 5, 20,
				20, "+"));

		// Refresh the research buttons.
		refreshResearch();
	}

	@SuppressWarnings("unchecked")
	public void refreshResearch() {
		// Grab the list of all research.
		List<ResearchItem> researches = Research.getCurrentResearch();

		// Reset the selected button.
		selectedButton = null;

		// Add buttons for all researches in range.
		for (int i = scrollPos; i < scrollPos + 8; i++) {
			int id = i - scrollPos + 1;
			int yPos = 6 + ((i - scrollPos) * 19);
			if (i < researches.size()) {
				buttonList.add(new ToggleButton(id, texture, guiLeft + 118,
						guiTop + yPos, 0, 166, 108, 19, fontRendererObj,
						researches.get(i)));
			} else {
				buttonList.add(new ToggleButton(id, texture, guiLeft + 118,
						guiTop + yPos, 0, 166, 108, 19, fontRendererObj));
			}
		}

		// Figure out whether to enable the scroll buttons.
		up.enabled = scrollPos > 0;
		down.enabled = researches.size() > scrollPos + 8;
	}

	protected void actionPerformed(GuiButton button) {
		// If it's a toggle button, select or deselect it.
		if (button instanceof ToggleButton) {
			ToggleButton t = (ToggleButton) button;
			t.down = !t.down;

			if (t.down) {
				if (selectedButton != null) {
					selectedButton.down = false;
				}
				selectedButton = t;
				researchIt.enabled = true;
			} else if (selectedButton == t) {
				selectedButton = null;
				researchIt.enabled = false;
			}
			// If it's the research button, research the selected item.
		} else if (button.id == researchIt.id) {
			if (selectedButton == null || selectedButton.research == null) {
				researchIt.enabled = false;
				return;
			}

			int researchLevel = selectedButton.research.iqsecs / 100;

			// Make sure the player has enough levels.
			if (this.mc.thePlayer.experienceLevel >= researchLevel) {
				this.mc.thePlayer.addExperienceLevel(-researchLevel);
				selectedButton.research.complete();
				this.mc.thePlayer.addChatMessage(new ChatComponentText(
						"Researched!"));
				refreshResearch();
			}
			// Or if it's the scrolly buttons, just scroll through it.
		} else if (button.id == up.id) {
			scrollPos--;
			refreshResearch();
		} else if (button.id == down.id) {
			scrollPos++;
			refreshResearch();
		}
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2,
			int var3) {
		// Just draw the background.
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(texture);
		this.drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	}

	/**
	 * Splits a string into pieces.
	 * 
	 * @param s
	 *            The string to dissect.
	 * @param width
	 *            The max width per piece.
	 * @return A chopped list of strings.
	 */
	protected List<String> dissect(String s, int width) {
		// Declare the array of "pieces"
		List<String> retval = new ArrayList<String>();

		// Split the string into whitespaced bits.
		String[] tokens = s.split(" ");

		String current = "";

		// Group the tokens into bits.
		for (int i = 0; i < tokens.length; i++) {
			current += tokens[i] + " ";
			if (fontRendererObj.getStringWidth(current) > width) {
				current = current.substring(0,
						current.length() - tokens[i].length() - 2);
				retval.add(current);
				current = "";
				i--;
			}
		}

		retval.add(current);

		return retval;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int param1, int param2) {
		// Display research data.
		if (selectedButton == null) {
			title = "Nothing Selected";
			body = "Click one of the available research items to begin!";
		} else {
			title = selectedButton.research.name;
			body = selectedButton.research.desc;
		}

		int titleX = (117 - fontRendererObj.getStringWidth(title)) / 2;

		fontRendererObj.drawString(title, guiLeft + titleX, guiTop + 5,
				0x404040);

		List<String> bodyPieces = dissect(body, 101);

		int y = guiTop + 5 + fontRendererObj.FONT_HEIGHT + 4 + 4;

		for (String s : bodyPieces) {
			fontRendererObj.drawString(s, guiLeft + 8, y, 0x404040);
			y += fontRendererObj.FONT_HEIGHT + 4;
		}
	}
}
