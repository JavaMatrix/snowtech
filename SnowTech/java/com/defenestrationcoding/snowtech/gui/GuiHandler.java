package com.defenestrationcoding.snowtech.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.defenestrationcoding.snowtech.SnowTech;
import com.defenestrationcoding.snowtech.containers.ContainerBroadcaster;
import com.defenestrationcoding.snowtech.containers.ContainerFrostbiteFurnace;
import com.defenestrationcoding.snowtech.containers.ContainerResearchTable;
import com.defenestrationcoding.snowtech.tileentities.TileEntityBroadcaster;
import com.defenestrationcoding.snowtech.tileentities.TileEntityFrostbiteFurnace;
import com.defenestrationcoding.snowtech.tileentities.TileEntityResearchTable;

import cpw.mods.fml.common.network.IGuiHandler;
import cpw.mods.fml.common.network.NetworkRegistry;

/**
 * Handles GUI requests.
 * @author JavaMatrix
 *
 */
public class GuiHandler implements IGuiHandler {

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world,
			int x, int y, int z) {
		// Grab the te.
		TileEntity te = world.getTileEntity(x, y, z);

		// Switch through and return the appropriate Container.
		switch (ID) {
		case GUI.RESEARCH_TABLE:
			if (te != null && te instanceof TileEntityResearchTable) {
				return new ContainerResearchTable();
			} else {
				return null;
			}
		case GUI.FROSTBITE_FURNACE:
			if (te != null && te instanceof TileEntityFrostbiteFurnace) {
				return new ContainerFrostbiteFurnace(player.inventory,
						(TileEntityFrostbiteFurnace) te);
			} else {
				return null;
			}
		case GUI.BROADCASTER:
			if (te != null && te instanceof TileEntityBroadcaster) {
				return new ContainerBroadcaster();
			} else {
				return null;
			}
		default:
			return null;
		}
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world,
			int x, int y, int z) {
		// Grab the te.
		TileEntity te = world.getTileEntity(x, y, z);

		// Switch through the IDs and return the appropriate GUI.
		switch (ID) {
		case GUI.RESEARCH_TABLE:
			if (te != null && te instanceof TileEntityResearchTable) {
				return new GuiResearchTable(player.inventory,
						(TileEntityResearchTable) te);
			} else {
				return null;
			}
		case GUI.FROSTBITE_FURNACE:
			if (te != null && te instanceof TileEntityFrostbiteFurnace) {
				return new GuiFrostbiteFurnace(player.inventory,
						(TileEntityFrostbiteFurnace) te);
			} else {
				return null;
			}
		case GUI.BROADCASTER:
			if (te != null && te instanceof TileEntityBroadcaster) {
				return new GuiBroadcaster(player.inventory,
						(TileEntityBroadcaster) te);
			} else {
				return null;
			}
		default:
			return null;
		}
	}

	public static void init() {
		NetworkRegistry.INSTANCE.registerGuiHandler(SnowTech.instance, new GuiHandler());
	}
}
