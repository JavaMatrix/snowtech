package com.defenestrationcoding.snowtech.gui;

/**
 * A list of constants denoting GUIs from the game.
 * @author JavaMatrix
 *
 */
public class GUI {
	public static final int RESEARCH_TABLE = 0;
	public static final int FROSTBITE_FURNACE = 1;
	public static final int BROADCASTER = 2;
}
