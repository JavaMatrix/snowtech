package com.defenestrationcoding.snowtech.gui;

import java.awt.Rectangle;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import com.defenestrationcoding.snowtech.research.ResearchItem;

/**
 * A GUI element that behaves as a toggle button.
 * 
 * @author JavaMatrix
 *
 */
public class ToggleButton extends GuiButton {
	// The location of the default button texture on the texture.
	Rectangle source;
	// The texture containing the button's images.
	ResourceLocation texture;
	// The research item represented by this toggle button.
	ResearchItem research;
	// The number of levels needed to research this item.
	int levels;
	// Whether or not this button is pressed down.
	boolean down;
	// The font renderer, used to render the text on the button.
	FontRenderer fontRendererObj;

	public ToggleButton(int id, ResourceLocation texture, int x, int y,
			int sourceX, int sourceY, int sourceWidth, int sourceHeight,
			FontRenderer fontRendererObj, ResearchItem research) {
		// Pass on the parameters to GuiButton.
		super(id, x, y, sourceWidth, sourceHeight, research.name);
		// Grab the texture.
		this.texture = texture;
		// Build the source rectangle.
		this.source = new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);
		// Calculate the number of levels needed to research this item.
		this.levels = research.iqsecs / 100;
		// The rest is just assignments.
		this.research = research;
		this.down = false;
		this.fontRendererObj = fontRendererObj;
	}
	
	public ToggleButton(int id, ResourceLocation texture, int x, int y,
			int sourceX, int sourceY, int sourceWidth, int sourceHeight,
			FontRenderer fontRendererObj) {
		// Just pass on the parameters to GuiButton.
		super(id, x, y, sourceWidth, sourceHeight, "");
		// Grab the texture.
		this.texture = texture;
		// Build the source rectangle.
		this.source = new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);
		// The rest is just assignments.
		this.levels = -1;
		this.research = null;
		this.down = false;
		this.fontRendererObj = fontRendererObj;
		this.enabled = false;
	}

	/**
	 * Determines whether the mouse is touching this button.
	 * @param mouseX The x position of the mouse.
	 * @param mouseY The y position of the mouse.
	 * @return True if the mouse is touching this button.
	 */
	public boolean mouseCollides(int mouseX, int mouseY) {
		// Calculate the edge positions.
		int top = yPosition;
		int left = xPosition;
		int bottom = yPosition + source.height;
		int right = xPosition + source.width;

		// Then check if the mouse is on the inside of each edge.
		return (mouseX > left && mouseX < right && mouseY > top && mouseY < bottom);
	}
	
	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY) {
		// Make a copy of source so we can shift it around
		// to reflect the state of the button.
		Rectangle where = new Rectangle(source);

		// If it's not enabled, move the texture down two slots.
		if (!this.enabled) {
			where.y += source.height * 2;
		} else {
			if (mouseCollides(mouseX, mouseY)) {
				// If the mouse is hovering over a pressed button,
				// move 3 slots down and 1 slot to the right.
				if (down) {
					where.y += source.height * 3;
					where.x += source.width;
				}
				// If the mouse is hovering over an unpressed button,
				// move 3 slots down.
				else {
					where.y += source.height * 3;
				}
				// Or if it's just down, move the texture down a single slot.
			} else if (down) {
				where.y += source.height;
			}
		}

		// Draw the button.
		GL11.glColor4f(1, 1, 1, 1);
		mc.renderEngine.bindTexture(texture);
		drawTexturedModalRect(xPosition, yPosition, where.x, where.y,
				where.width, where.height);

		// Put various research data on the button.
		if (research != null) {
			// The name for one.
			fontRendererObj.drawString(research.name, xPosition + 2,
					yPosition + 2, 0xFFFFFFFF);

			if (levels != -1 && levels < 100) {
				// And the number of levels required, for another.
				fontRendererObj.drawStringWithShadow(
						Integer.toString(levels),
						xPosition + source.width
								- fontRendererObj.getStringWidth(Integer.toString(levels))
								- 3, yPosition + source.height
								- fontRendererObj.FONT_HEIGHT - 1, 0x00FF00);
			}
		}
	}
}
