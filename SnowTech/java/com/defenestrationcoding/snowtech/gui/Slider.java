package com.defenestrationcoding.snowtech.gui;

import java.awt.Rectangle;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

/**
 * A slider for adjusting analog values in a GUI.
 * 
 * @author JavaMatrix
 * 
 */
public class Slider extends GuiButton {
	// The location where the slider's texture is stored.
	// May contain other GUI elements as well.
	public ResourceLocation texture;
	// The rectangle within the image that stores the slider's graphics.
	Rectangle source;
	// The maximum and minimum slider positions.
	public int maxX;
	public int minX;
	// Whether or not the mouse is currently down.
	public boolean mouseDown = false;
	// The mouse position relative to the slider's left side.
	public int mouseRelX = 0;

	public Slider(int id, ResourceLocation texture, int x, int y, int sourceX,
			int sourceY, int sourceWidth, int sourceHeight, int minX, int maxX) {
		// No name, but just pass the rest on up to GuiButton().
		super(id, x, y, sourceWidth, sourceHeight, "");
		// Grab the texture.
		this.texture = texture;
		// Construct the source rectangle.
		this.source = new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);
		// The rest is just plain old assignments.
		this.width = sourceWidth;
		this.height = sourceHeight;
		this.maxX = maxX;
		this.minX = minX;
	}

	/**
	 * Determines if the mouse's point is within this slider.
	 * 
	 * @param mouseX
	 *            The x position of the mouse's point.
	 * @param mouseY
	 *            The y position of the mouse's point.
	 * @return True if the mouse is within this slider.
	 */
	public boolean mouseCollides(int mouseX, int mouseY) {
		// Find out the edge coordinates.
		int top = yPosition;
		int left = xPosition;
		int bottom = yPosition + source.height;
		int right = xPosition + source.width;

		// Then check if the mouse is inside of the rectangle.
		return (mouseX > left && mouseX < right && mouseY > top && mouseY < bottom);
	}

	/**
	 * Called automagically when the mouse is pressed in the parent GUI.
	 */
	@Override
	public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
		boolean hit = super.mousePressed(mc, mouseX, mouseY);

		// If we've been hit, then the mouse is now pressing
		// on this slider. Grab the relative position.
		if (hit) {
			mouseDown = true;
			mouseRelX = xPosition - mouseX;
		}

		// And pass on the result as if the rest never happened xD
		return hit;
	}

	/**
	 * Called automagically when the mouse is let off of this control.
	 */
	@Override
	public void mouseReleased(int mouseX, int mouseY) {
		// The mouse is dead, hurrah!
		mouseDown = false;
		// Set the xPosition to the right spot.
		xPosition = mouseX + mouseRelX;
		xPosition = Math.max(xPosition, minX);
		xPosition = Math.min(xPosition, maxX);
	}

	/**
	 * Called to update the slider's position.
	 * @param mouseX
	 */
	public void update(int mouseX) {
		// Unfortunately we have to check it this way because
		// Minecraft can be rather picky.  This is LWJGL code BTW.
		mouseDown = Mouse.isButtonDown(0);

		// Update the slider position if the mouse is indeed down.
		if (mouseDown) {
			xPosition = mouseX + mouseRelX;
			xPosition = Math.max(xPosition, minX);
			xPosition = Math.min(xPosition, maxX);
		}
	}

	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY) {
		// We have to update here for lack of a
		// better place.
		update(mouseX);
		
		// Draw the pretty little button.
		GL11.glColor4f(1, 1, 1, 1);
		mc.renderEngine.bindTexture(texture);
		drawTexturedModalRect(xPosition, yPosition, source.x, source.y,
				source.width, source.height);
	}

	public double getPercentage() {
		// Just some math to get the position as a percentage.
		return (double) (xPosition - minX) / (double) (maxX - minX);
	}

	public void moveToPercentage(double percentage) {
		// More math for making the slider move to a given percentage.
		xPosition = (int) (minX + (percentage * (maxX - minX)));
	}
}
