package com.defenestrationcoding.snowtech.gui;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import com.defenestrationcoding.snowtech.SnowTech;
import com.defenestrationcoding.snowtech.blocks.BlockBroadcaster;
import com.defenestrationcoding.snowtech.containers.ContainerBroadcaster;
import com.defenestrationcoding.snowtech.tileentities.TileEntityBroadcaster;

/**
 * The gui for the {@link BlockBroadcaster}.
 * 
 * @author JavaMatrix
 * 
 */
public class GuiBroadcaster extends GuiContainer {
	// The texture for the GUI.
	private ResourceLocation texture = new ResourceLocation(SnowTech.modid,
			"textures/gui/broadcaster.png");
	// The tile entity bound to this GUI.
	private TileEntityBroadcaster te;
	// The slider for choosing the signal to output.
	private Slider slider;

	/**
	 * The constructyor
	 * 
	 * @param inventoryplayer
	 *            The player's inventory.
	 * @param tileentity
	 *            The tile entity to bind to the GUI.
	 */
	public GuiBroadcaster(InventoryPlayer inventoryplayer,
			TileEntityBroadcaster tileentity) {
		// Do the default GUI constructor stuff.
		super(new ContainerBroadcaster());
		// Bind the tile entity.
		te = tileentity;
		// Set the GUI size.
		xSize = 138;
		ySize = 41;
	}

	@SuppressWarnings("unchecked")
	public void initGui() {
		// The top and left sides of the GUI, in pixels.
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;

		// Set the slider's start position.
		int sliderX;
		if (te.sliderX == -1) {
			sliderX = guiLeft + 25;
		} else {
			sliderX = te.sliderX;
		}

		// Initialize the slider, and put it on the button list.
		slider = new Slider(0, texture, sliderX, guiTop + 7, 0, 45, 12, 12,
				guiLeft + 25, guiLeft + 101);
		buttonList.add(slider);
	}

	protected void drawGuiContainerForegroundLayer(int par1, int par2) {
		// Draw the current signal being broadcast.
		NumberFormat formatter = new DecimalFormat("000");
		String number = formatter.format(te.signal);
		fontRendererObj.drawString(number + " Notches", 20, 25, 0xFFFFFF);
	}

	protected void drawGuiContainerBackgroundLayer(float f, int var1, int var2) {
		// Draw the background.
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture(texture);
		int x = (width - xSize) / 2;
		int y = (height - ySize) / 2;
		drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
		
		// Update the slider and signal.
		te.signal = (int) (slider.getPercentage() * 255);
		te.sliderX = slider.xPosition;
	}

}
