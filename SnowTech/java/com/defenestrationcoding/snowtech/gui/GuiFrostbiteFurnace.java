package com.defenestrationcoding.snowtech.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

import com.defenestrationcoding.snowtech.SnowTech;
import com.defenestrationcoding.snowtech.containers.ContainerFrostbiteFurnace;
import com.defenestrationcoding.snowtech.tileentities.TileEntityFrostbiteFurnace;

/**
 * The GUI for the {@link BlockFrostbiteFurnace}.
 * @author JavaMatrix
 *
 */
public class GuiFrostbiteFurnace extends GuiContainer {
	
	// The background texture.
	private ResourceLocation texture = new ResourceLocation(SnowTech.modid, "textures/gui/frostbitefurnace.png");
	// The TE that this GUI is bound to.
	private TileEntityFrostbiteFurnace furnaceInventory;
	
	/**
	 * The constructor
	 * @param inventoryplayer The player's inventory.
	 * @param tileentity The TE to bind to.
	 */
	public GuiFrostbiteFurnace(InventoryPlayer inventoryplayer,
			TileEntityFrostbiteFurnace tileentity) {
		// Run the super constructor.
		super(
				new ContainerFrostbiteFurnace(inventoryplayer,
						tileentity));
		// Bind the TE.
		furnaceInventory = tileentity;
	}

	protected void drawGuiContainerForegroundLayer(int par1, int par2) {
		// Draw the title for the furnace and the player's inventory.
		fontRendererObj.drawString(
				StatCollector.translateToLocal("Frostbite Furnace"), 50, 4,
				0x0000FF);
		fontRendererObj.drawString(StatCollector.translateToLocal("Inventory"),
				7, (ySize - 94) + 2, 0x0000FF);
	}

	protected void drawGuiContainerBackgroundLayer(float f, int var1, int var2) {
		// Draw the background.
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture(texture);
		int x = (width - xSize) / 2;
		int y = (height - ySize) / 2;
		drawTexturedModalRect(x, y, 0, 0, xSize, ySize);

		// Draw the fire if applicable.
		if (furnaceInventory.isBurning()) {
			int size = furnaceInventory.getBurnTimeRemainingScaled(12);
			drawTexturedModalRect(x + 56, y + 36 + 12 - size, 176, 12 - size, 14, size + 2);
		}
		
		// Draw the progress arrow.
		int progress = furnaceInventory.getCookProgressScaled(24);
		drawTexturedModalRect(x + 79, y + 34, 176, 14, progress + 1, 16);
	}
}
