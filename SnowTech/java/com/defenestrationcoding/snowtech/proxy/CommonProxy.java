package com.defenestrationcoding.snowtech.proxy;

import com.defenestrationcoding.snowtech.commands.CommandKillWater;

import cpw.mods.fml.common.event.FMLServerStartingEvent;

public class CommonProxy {

	// Client stuff
	public void registerRenderers() {
		// Nothing here as the server doesn't render graphics or entities!
	}
	
	public void onServerStarting(FMLServerStartingEvent ev) {
		ev.registerServerCommand(new CommandKillWater());
	}
}