package com.defenestrationcoding.snowtech.proxy;

import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;
import tconstruct.tools.entity.FancyEntityItem;
import tconstruct.tools.model.FancyItemRender;

import com.defenestrationcoding.snowtech.projectiles.EntityUnclipperBullet;
import com.defenestrationcoding.snowtech.renderers.RenderUCBullet;
import com.defenestrationcoding.snowtech.renderers.RendererBroadcaster;
import com.defenestrationcoding.snowtech.renderers.RendererHaywire;
import com.defenestrationcoding.snowtech.renderers.RendererHaywireItem;
import com.defenestrationcoding.snowtech.renderers.RendererReceiver;
import com.defenestrationcoding.snowtech.renderers.RendererResearchTable;
import com.defenestrationcoding.snowtech.renderers.RendererResearchTableItem;
import com.defenestrationcoding.snowtech.renderers.RendererResearcherFred;
import com.defenestrationcoding.snowtech.renderers.RendererResearcherFredItem;
import com.defenestrationcoding.snowtech.renderers.RendererSmithy;
import com.defenestrationcoding.snowtech.tileentities.TileEntityBroadcaster;
import com.defenestrationcoding.snowtech.tileentities.TileEntityHaywire;
import com.defenestrationcoding.snowtech.tileentities.TileEntityReceiver;
import com.defenestrationcoding.snowtech.tileentities.TileEntityResearchTable;
import com.defenestrationcoding.snowtech.tileentities.TileEntityResearcherFred;
import com.defenestrationcoding.snowtech.tileentities.TileEntitySmithy;
import com.defenestrationcoding.snowtech.util.Registration.SnowTechBlocks;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy {

	@Override
	public void registerRenderers() {
		ClientRegistry.bindTileEntitySpecialRenderer(
				TileEntityResearchTable.class, new RendererResearchTable());
		ClientRegistry.bindTileEntitySpecialRenderer(
				TileEntityResearcherFred.class, new RendererResearcherFred());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityHaywire.class,
				new RendererHaywire());
		ClientRegistry.bindTileEntitySpecialRenderer(
				TileEntityBroadcaster.class, new RendererBroadcaster());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityReceiver.class,
				new RendererReceiver());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySmithy.class,
				new RendererSmithy());
		RenderingRegistry.registerEntityRenderingHandler(FancyEntityItem.class,
				new FancyItemRender());
		MinecraftForgeClient.registerItemRenderer(
				Item.getItemFromBlock(SnowTechBlocks.blockResearchTable),
				new RendererResearchTableItem());
		MinecraftForgeClient.registerItemRenderer(
				Item.getItemFromBlock(SnowTechBlocks.blockResearcherFred),
				new RendererResearcherFredItem());
		MinecraftForgeClient.registerItemRenderer(
				Item.getItemFromBlock(SnowTechBlocks.blockHaywire),
				new RendererHaywireItem());
		RenderingRegistry.registerEntityRenderingHandler(
				EntityUnclipperBullet.class, new RenderUCBullet());
	}
}