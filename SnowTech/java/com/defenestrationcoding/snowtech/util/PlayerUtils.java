package com.defenestrationcoding.snowtech.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.ChatComponentText;

import com.defenestrationcoding.snowtech.util.Registration.SnowTechItems;

public class PlayerUtils {
	public static boolean isHolding(EntityPlayer p, Item i) {
		return p.getHeldItem() != null && p.getHeldItem().getItem().equals(i);
	}
	
	public static boolean hasDebugger(EntityPlayer p) {
		return SnowTechItems.itemDebugger != null && isHolding(p, SnowTechItems.itemDebugger);
	}
	
	public static void sendChat(EntityPlayer player, String message) {
		player.addChatMessage(new ChatComponentText(message));
	}
}
