package com.defenestrationcoding.snowtech.util;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.oredict.OreDictionary;

import com.defenestrationcoding.snowtech.SnowTech;
import com.defenestrationcoding.snowtech.blocks.BlockBroadcaster;
import com.defenestrationcoding.snowtech.blocks.BlockFallingWater;
import com.defenestrationcoding.snowtech.blocks.BlockFrostbiteFurnace;
import com.defenestrationcoding.snowtech.blocks.BlockHaywire;
import com.defenestrationcoding.snowtech.blocks.BlockPump;
import com.defenestrationcoding.snowtech.blocks.BlockReceiver;
import com.defenestrationcoding.snowtech.blocks.BlockResearchTable;
import com.defenestrationcoding.snowtech.blocks.BlockResearcherFred;
import com.defenestrationcoding.snowtech.blocks.BlockSmithy;
import com.defenestrationcoding.snowtech.blocks.BlockSpout;
import com.defenestrationcoding.snowtech.blocks.BlockTuner;
import com.defenestrationcoding.snowtech.items.ItemDeathPack;
import com.defenestrationcoding.snowtech.items.ItemSundial;
import com.defenestrationcoding.snowtech.items.ItemSwordOfMendragor;
import com.defenestrationcoding.snowtech.items.ItemUnclipperGun;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMagicalAxe;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMagicalHammer;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMagicalHoe;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMagicalIngot;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMagicalPickaxe;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMagicalShovel;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMagicalSword;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMetalAxe;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMetalHammer;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMetalHoe;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMetalIngot;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMetalPickaxe;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMetalShovel;
import com.defenestrationcoding.snowtech.items.hardtools.ItemHardenedMetalSword;
import com.defenestrationcoding.snowtech.projectiles.EntityUnclipperBullet;
import com.defenestrationcoding.snowtech.tileentities.TileEntityBroadcaster;
import com.defenestrationcoding.snowtech.tileentities.TileEntityFallingWater;
import com.defenestrationcoding.snowtech.tileentities.TileEntityFrostbiteFurnace;
import com.defenestrationcoding.snowtech.tileentities.TileEntityHaywire;
import com.defenestrationcoding.snowtech.tileentities.TileEntityReceiver;
import com.defenestrationcoding.snowtech.tileentities.TileEntityResearchTable;
import com.defenestrationcoding.snowtech.tileentities.TileEntityResearcherFred;
import com.defenestrationcoding.snowtech.tileentities.TileEntitySmithy;
import com.defenestrationcoding.snowtech.tileentities.TileEntitySpout;
import com.defenestrationcoding.snowtech.tileentities.TileEntityTuner;

import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * Handles block, item, and TileEntity registration.
 * 
 * @author JavaMatrix
 * 
 */
public class Registration {

	public static class SnowTechItems {
		// All of the items to be registered.
		public static Item itemTelepathicSnow;
		public static Item itemDebugger;
		public static Item gear;
		public static Item sundial;
		public static Item swordOfMendragor;
		public static Item deathPack;
		public static Item stepBoots;
		public static Item unclipper;
		public static Item enthalpiteShard;
		public static Item enthalpite;
		public static Item moltenIngot;
		public static Item hammerStone;
		public static Item hammerIron;
		public static Item hammerGold;
		public static Item hammerDiamond;
		public static Item hammerAdmin;
		public static Item hardenedMetalIngot;
		public static Item hardenedMagicalIngot;
		public static Item hardenedMetalPickaxe;
		public static Item hardenedMetalHoe;
		public static Item hardenedMetalAxe;
		public static Item hardenedMetalShovel;
		public static Item hardenedMetalSword;
		public static Item hardenedMetalHammer;
		public static Item hardenedMagicalPickaxe;
		public static Item hardenedMagicalHoe;
		public static Item hardenedMagicalAxe;
		public static Item hardenedMagicalShovel;
		public static Item hardenedMagicalSword;
		public static Item hardenedMagicalHammer;
	}

	public static class SnowTechBlocks {
		// All of the blocks to be registered.
		public static Block blockTuner;
		public static Block blockResearchTable;
		public static Block blockFrostbiteFurnace;
		public static Block blockResearcherFred;
		public static Block blockHaywire;
		public static Block blockBroadcaster;
		public static Block blockReceiver;
		public static Block blockFallingWater;
		public static Block blockPump;
		public static Block blockSpout;
		public static Block blockSmithy;
	}

	// The creative tab for the mod.
	public static CreativeTabs snowTechCreativeTab = new CreativeTabs(
			"snowTechCreativeTab") {
		public Item getTabIconItem() {
			return SnowTechItems.itemTelepathicSnow;
		}
	};

	/**
	 * Registers all the items in the mod.
	 */
	public static void registerItems() {
		// All these items are pretty much the same.
		// Create a new instance of the item class and set its creative tab.
		SnowTechItems.itemTelepathicSnow = new Item()
				.setCreativeTab(snowTechCreativeTab)
				// Set an unlocalized name (camelCase of the English name
				// mostly).
				.setUnlocalizedName("telepathicSnow")
				// Set a texture name (SnowTech.modid + ":" + camelCase of the
				// English
				// name).
				.setTextureName(SnowTech.modid + ":" + "telepathicSnow");
		// And register it in the game registry.
		GameRegistry.registerItem(SnowTechItems.itemTelepathicSnow,
				"telepathicSnow");

		SnowTechItems.itemDebugger = new Item()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("itemDebugger")
				.setTextureName(SnowTech.modid + ":" + "itemDebugger");
		GameRegistry.registerItem(SnowTechItems.itemDebugger, "itemDebugger");

		SnowTechItems.gear = new Item().setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("gear")
				.setTextureName(SnowTech.modid + ":" + "gear");
		GameRegistry.registerItem(SnowTechItems.gear, "gear");

		SnowTechItems.sundial = new ItemSundial()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("sundial")
				.setTextureName(SnowTech.modid + ":" + "sundial");
		GameRegistry.registerItem(SnowTechItems.sundial, "sundial");

		// This tool material is used for the Sword of Mendragor.
		Item.ToolMaterial mendragorium = EnumHelper.addToolMaterial(
				"mendragorium", 0, 0, 0, Float.POSITIVE_INFINITY, 0);

		SnowTechItems.swordOfMendragor = new ItemSwordOfMendragor(mendragorium)
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("swordOfMendragor")
				.setTextureName(SnowTech.modid + ":mendragorSword");
		GameRegistry.registerItem(SnowTechItems.swordOfMendragor,
				"swordOfMendragor");

		// The armor values and the durability for the Death Pack.
		ItemArmor.ArmorMaterial deathPackMaterial = EnumHelper
				.addArmorMaterial("deathpack", 2000 / 16, new int[] { 2, 7, 5,
						1 }, 0);

		SnowTechItems.deathPack = new ItemDeathPack(deathPackMaterial, 1)
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("deathPack")
				.setTextureName(SnowTech.modid + ":deathpack");
		GameRegistry.registerItem(SnowTechItems.deathPack, "deathPacK");

		SnowTechItems.stepBoots = new ItemDeathPack(deathPackMaterial, 3)
				.setCreativeTab(snowTechCreativeTab).setUnlocalizedName(
						"stepBoots");
		GameRegistry.registerItem(SnowTechItems.stepBoots, "stepBoots");

		SnowTechItems.unclipper = new ItemUnclipperGun()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("unclipper").setMaxDamage(1000)
				.setTextureName(SnowTech.modid + ":unclipper");
		GameRegistry.registerItem(SnowTechItems.unclipper, "unclipper");

		SnowTechItems.enthalpiteShard = new Item()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("enthalpiteShard")
				.setTextureName(SnowTech.modid + ":enthalpiteShard");
		GameRegistry.registerItem(SnowTechItems.enthalpiteShard,
				"enthalpiteShard");

		SnowTechItems.enthalpite = new Item()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("enthalpite")
				.setTextureName(SnowTech.modid + ":enthalpite");
		GameRegistry.registerItem(SnowTechItems.enthalpite, "enthalpite");

		SnowTechItems.moltenIngot = new Item()
				.setUnlocalizedName("moltenIngot")
				.setTextureName(SnowTech.modid + ":moltenIngot")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.moltenIngot, "moltenIngot");

		SnowTechItems.hammerStone = new Item()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hammerStone")
				.setTextureName(SnowTech.modid + ":hammerStone")
				.setMaxStackSize(1).setMaxDamage(131);
		GameRegistry.registerItem(SnowTechItems.hammerStone, "hammerStone");

		SnowTechItems.hammerIron = new Item()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hammerIron")
				.setTextureName(SnowTech.modid + ":hammerIron")
				.setMaxStackSize(1).setMaxDamage(250);
		GameRegistry.registerItem(SnowTechItems.hammerIron, "hammerIron");

		SnowTechItems.hammerGold = new Item()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hammerGold")
				.setTextureName(SnowTech.modid + ":hammerGold")
				.setMaxStackSize(1).setMaxDamage(32);
		GameRegistry.registerItem(SnowTechItems.hammerGold, "hammerGold");

		SnowTechItems.hammerDiamond = new Item()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hammerDiamond")
				.setTextureName(SnowTech.modid + ":hammerDiamond")
				.setMaxStackSize(1).setMaxDamage(1561);
		GameRegistry.registerItem(SnowTechItems.hammerDiamond, "hammerDiamond");

		SnowTechItems.hammerAdmin = new Item()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hammerAdmin")
				.setTextureName(SnowTech.modid + ":hammerAdmin")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hammerAdmin, "hammerAdmin");

		SnowTechItems.hardenedMetalIngot = new ItemHardenedMetalIngot()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMetalIngot")
				.setTextureName(
						SnowTech.modid + ":hardenedMetal/hardenedMetalIngot")
				.setMaxDamage(0).setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMetalIngot,
				"hardenedMetalIngot");

		SnowTechItems.hardenedMagicalIngot = new ItemHardenedMagicalIngot()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMagicalIngot")
				.setTextureName(SnowTech.modid + ":hardenedMagicalIngot")
				.setMaxDamage(0).setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMagicalIngot,
				"hardenedMagicalIngot");

		SnowTechItems.hardenedMetalPickaxe = new ItemHardenedMetalPickaxe()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMetalPickaxe")
				.setTextureName(
						SnowTech.modid + ":hardenedMetal/hardenedMetalPickaxe")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMetalPickaxe,
				"hardenedMetalPickaxe");

		SnowTechItems.hardenedMetalHoe = new ItemHardenedMetalHoe()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMetalHoe")
				.setTextureName(
						SnowTech.modid + ":hardenedMetal/hardenedMetalHoe")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMetalHoe,
				"hardenedMetalHoe");

		SnowTechItems.hardenedMetalAxe = new ItemHardenedMetalAxe()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMetalAxe")
				.setTextureName(
						SnowTech.modid + ":hardenedMetal/hardenedMetalAxe")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMetalAxe,
				"hardenedMetalAxe");

		SnowTechItems.hardenedMetalShovel = new ItemHardenedMetalShovel()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMetalShovel")
				.setTextureName(
						SnowTech.modid + ":hardenedMetal/hardenedMetalShovel")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMetalShovel,
				"hardenedMetalShovel");

		SnowTechItems.hardenedMetalSword = new ItemHardenedMetalSword()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMetalSword")
				.setTextureName(
						SnowTech.modid + ":hardenedMetal/hardenedMetalSword")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMetalSword,
				"hardenedMetalSword");

		SnowTechItems.hardenedMetalHammer = new ItemHardenedMetalHammer()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMetalHammer")
				.setTextureName(
						SnowTech.modid + ":hardenedMetal/hardenedMetalHammer")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMetalHammer,
				"hardenedMetalHammer");

		SnowTechItems.hardenedMagicalPickaxe = new ItemHardenedMagicalPickaxe()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMagicalPickaxe")
				.setTextureName(
						SnowTech.modid
								+ ":hardenedMetal/hardenedMagicalPickaxe")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMagicalPickaxe,
				"hardenedMagicalPickaxe");

		SnowTechItems.hardenedMagicalHoe = new ItemHardenedMagicalHoe()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMagicalHoe")
				.setTextureName(
						SnowTech.modid + ":hardenedMetal/hardenedMagicalHoe")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMagicalHoe,
				"hardenedMagicalHoe");

		SnowTechItems.hardenedMagicalAxe = new ItemHardenedMagicalAxe()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMagicalAxe")
				.setTextureName(
						SnowTech.modid + ":hardenedMetal/hardenedMagicalAxe")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMagicalAxe,
				"hardenedMagicalAxe");

		SnowTechItems.hardenedMagicalShovel = new ItemHardenedMagicalShovel()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMagicalShovel")
				.setTextureName(
						SnowTech.modid + ":hardenedMetal/hardenedMagicalShovel")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMagicalShovel,
				"hardenedMagicalShovel");

		SnowTechItems.hardenedMagicalSword = new ItemHardenedMagicalSword()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMagicalSword")
				.setTextureName(
						SnowTech.modid + ":hardenedMetal/hardenedMagicalSword")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMagicalSword,
				"hardenedMagicalSword");

		SnowTechItems.hardenedMagicalHammer = new ItemHardenedMagicalHammer()
				.setCreativeTab(snowTechCreativeTab)
				.setUnlocalizedName("hardenedMagicalHammer")
				.setTextureName(
						SnowTech.modid + ":hardenedMetal/hardenedMagicalHammer")
				.setMaxStackSize(1);
		GameRegistry.registerItem(SnowTechItems.hardenedMagicalHammer,
				"hardenedMagicalHammer");
	}

	/**
	 * Registers all the blocks and TileEntities for the mod.
	 */
	public static void registerBlocks() {
		// Again, these are pretty much all the same.
		// Make a new block,
		// Set it's name (camelCase of the English name),
		// Set the creative tab,
		// And register it.
		SnowTechBlocks.blockTuner = new BlockTuner().setBlockName("tuner")
				.setCreativeTab(snowTechCreativeTab);
		GameRegistry.registerBlock(SnowTechBlocks.blockTuner, "tuner");
		GameRegistry.registerTileEntity(TileEntityTuner.class, "teTuner");

		SnowTechBlocks.blockResearchTable = new BlockResearchTable()
				.setBlockName("researchTable").setCreativeTab(
						snowTechCreativeTab);
		GameRegistry.registerBlock(SnowTechBlocks.blockResearchTable,
				"researchTable");
		// Some of these have TileEntities, which I name like te + camelCase of
		// the English name.
		GameRegistry.registerTileEntity(TileEntityResearchTable.class,
				"teResearchTable");

		SnowTechBlocks.blockFrostbiteFurnace = new BlockFrostbiteFurnace()
				.setBlockName("frostbiteFurnace").setCreativeTab(
						snowTechCreativeTab);
		GameRegistry.registerBlock(SnowTechBlocks.blockFrostbiteFurnace,
				"frostbiteFurnace");
		GameRegistry.registerTileEntity(TileEntityFrostbiteFurnace.class,
				"teFrostbiteFurnace");

		SnowTechBlocks.blockResearcherFred = new BlockResearcherFred()
				.setBlockName("researcherFred")
				// Fred glows a bit, but he's not that bright...
				.setCreativeTab(snowTechCreativeTab).setLightLevel(0.1f);

		GameRegistry.registerBlock(SnowTechBlocks.blockResearcherFred,
				"researcherFred");
		GameRegistry.registerTileEntity(TileEntityResearcherFred.class,
				"teResearcherFred");

		SnowTechBlocks.blockHaywire = new BlockHaywire()
				.setBlockName("haywire").setCreativeTab(snowTechCreativeTab);
		GameRegistry.registerBlock(SnowTechBlocks.blockHaywire, "haywire");
		GameRegistry.registerTileEntity(TileEntityHaywire.class, "teHaywire");

		SnowTechBlocks.blockBroadcaster = new BlockBroadcaster()
				.setBlockName("broadcaster")
				.setCreativeTab(snowTechCreativeTab)
				// I have to specify block texture on some, this works the same
				// as
				// item textures for naming.
				.setBlockTextureName(SnowTech.modid + ":blockBroadcaster");
		GameRegistry.registerBlock(SnowTechBlocks.blockBroadcaster,
				"broadcaster");
		GameRegistry.registerTileEntity(TileEntityBroadcaster.class,
				"teBroadcaster");

		SnowTechBlocks.blockReceiver = new BlockReceiver()
				.setBlockName("receiver").setCreativeTab(snowTechCreativeTab)
				.setBlockTextureName(SnowTech.modid + ":blockReceiver");
		GameRegistry.registerBlock(SnowTechBlocks.blockReceiver, "receiver");
		GameRegistry.registerTileEntity(TileEntityReceiver.class, "teReceiver");

		SnowTechBlocks.blockFallingWater = new BlockFallingWater(null)
				.setBlockName("fallingWater");
		GameRegistry.registerBlock(SnowTechBlocks.blockFallingWater,
				"fallingWater");
		GameRegistry.registerTileEntity(TileEntityFallingWater.class,
				"teFallingWater");

		SnowTechBlocks.blockPump = new BlockPump().setBlockName("pump")
				.setCreativeTab(snowTechCreativeTab);
		GameRegistry.registerBlock(SnowTechBlocks.blockPump, "pump");

		SnowTechBlocks.blockSpout = new BlockSpout().setBlockName("spout")
				.setCreativeTab(snowTechCreativeTab);
		GameRegistry.registerBlock(SnowTechBlocks.blockSpout, "spout");
		GameRegistry.registerTileEntity(TileEntitySpout.class, "teSpout");

		SnowTechBlocks.blockSmithy = new BlockSmithy().setBlockName("smithy")
				.setCreativeTab(snowTechCreativeTab);
		GameRegistry.registerBlock(SnowTechBlocks.blockSmithy, "smithy");
		GameRegistry.registerTileEntity(TileEntitySmithy.class, "teSmithy");
	}

	/**
	 * Registers all manner of recipes for the mod.
	 */
	public static void registerRecipes() {
		// Register the vanilla recipe for Telepathic Snow.
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.itemTelepathicSnow,
				4), "rsr", "srs", "rsr", 'r', new ItemStack(Items.redstone),
				's', new ItemStack(Items.snowball));

		// Register the recipe for the research table (that's important!)
		GameRegistry.addRecipe(new ItemStack(SnowTechBlocks.blockResearchTable,
				1), "ppp", "WWW", "s s", 'p', new ItemStack(Items.paper), 'W',
				new ItemStack(Blocks.planks), 's', new ItemStack(Items.stick));

		// Register the recipe for a gear.
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.gear, 1), " s ",
				"s s", " s ", 's', new ItemStack(Items.stick));

		// Register the recipe for a sundial.
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.sundial, 1), "s",
				"W", 's', new ItemStack(Items.stick), 'W', new ItemStack(
						Blocks.planks));

		// Add the smelting recipe for a Shard of Enthalpite.
		GameRegistry.addSmelting(Items.blaze_rod, new ItemStack(
				SnowTechItems.enthalpiteShard), 1);

		// Add the crafting/decrafting recipe for an Enthalpite Crystal.
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.enthalpite), "eee",
				"eee", "eee", 'e', SnowTechItems.enthalpiteShard);
		GameRegistry.addShapelessRecipe(new ItemStack(
				SnowTechItems.enthalpiteShard, 9), new ItemStack(
				SnowTechItems.enthalpite));

		// Basic pickaxe recipe for the hardened metal pickaxe.
		GameRegistry.addRecipe(
				new ItemStack(SnowTechItems.hardenedMetalPickaxe), "hhh",
				" s ", " s ", 'h', new ItemStack(
						SnowTechItems.hardenedMetalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);

		// Basic hoe recipe for the hardened metal hoe.
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hardenedMetalHoe),
				"hh", " s", " s", 'h', new ItemStack(
						SnowTechItems.hardenedMetalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hardenedMetalHoe),
				"hh", "s ", "s ", 'h', new ItemStack(
						SnowTechItems.hardenedMetalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);
		// Basic axe recipes for the hardened metal axe.
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hardenedMetalAxe),
				"hh", "hs", " s", 'h', new ItemStack(
						SnowTechItems.hardenedMetalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hardenedMetalAxe),
				"hh", "sh", "s ", 'h', new ItemStack(
						SnowTechItems.hardenedMetalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);
		// Basic shovel recipe for the hardened metal shovel
		GameRegistry.addRecipe(
				new ItemStack(SnowTechItems.hardenedMetalShovel), "h", "s",
				"s", 'h', new ItemStack(SnowTechItems.hardenedMetalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);

		// Basic sword recipe for the hardened metal sword
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hardenedMetalSword),
				"h", "h", "s", 'h', new ItemStack(
						SnowTechItems.hardenedMetalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);

		// Basic pickaxe recipe for the hardened magical pickaxe.
		GameRegistry.addRecipe(new ItemStack(
				SnowTechItems.hardenedMagicalPickaxe), "hhh", " s ", " s ",
				'h', new ItemStack(SnowTechItems.hardenedMagicalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);

		// Basic hoe recipe for the hardened magical hoe.
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hardenedMagicalHoe),
				"hh", " s", " s", 'h', new ItemStack(
						SnowTechItems.hardenedMagicalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hardenedMagicalHoe),
				"hh", "s ", "s ", 'h', new ItemStack(
						SnowTechItems.hardenedMagicalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);
		// Basic axe recipes for the hardened magical axe.
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hardenedMagicalAxe),
				"hh", "hs", " s", 'h', new ItemStack(
						SnowTechItems.hardenedMagicalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hardenedMagicalAxe),
				"hh", "sh", "s ", 'h', new ItemStack(
						SnowTechItems.hardenedMagicalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);
		// Basic shovel recipe for the hardened magical shovel
		GameRegistry.addRecipe(new ItemStack(
				SnowTechItems.hardenedMagicalShovel), "h", "s", "s", 'h',
				new ItemStack(SnowTechItems.hardenedMagicalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);

		// Basic sword recipe for the hardened magical sword
		GameRegistry.addRecipe(
				new ItemStack(SnowTechItems.hardenedMagicalSword), "h", "h",
				"s", 'h', new ItemStack(SnowTechItems.hardenedMagicalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);

		// Hammer recipes
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hammerStone), " ii",
				"is ", " s ", 'i', Blocks.cobblestone, 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hammerStone), "ii ",
				" si", " s ", 'i', Blocks.cobblestone, 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hammerIron), " ii",
				"is ", " s ", 'i', Items.iron_ingot, 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hammerIron), "ii ",
				" si", " s ", 'i', Items.iron_ingot, 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hammerGold), " ii",
				"is ", " s ", 'i', Items.gold_ingot, 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hammerGold), "ii ",
				" si", " s ", 'i', Items.gold_ingot, 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hammerDiamond), " ii",
				"is ", " s ", 'i', Items.diamond, 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(SnowTechItems.hammerDiamond), "ii ",
				" si", " s ", 'i', Items.diamond, 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(
				SnowTechItems.hardenedMagicalHammer), " ii", "is ", " s ", 'i',
				new ItemStack(SnowTechItems.hardenedMagicalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(
				SnowTechItems.hardenedMagicalHammer), "ii ", " si", " s ", 'i',
				new ItemStack(SnowTechItems.hardenedMagicalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);
		GameRegistry.addRecipe(
				new ItemStack(SnowTechItems.hardenedMetalHammer), " ii", "is ",
				" s ", 'i', new ItemStack(SnowTechItems.hardenedMetalIngot,
						1, OreDictionary.WILDCARD_VALUE), 's', Items.stick);
		GameRegistry.addRecipe(new ItemStack(
				SnowTechItems.hardenedMetalHammer), "ii ", " si", " s ", 'i',
				new ItemStack(SnowTechItems.hardenedMetalIngot, 1,
						OreDictionary.WILDCARD_VALUE), 's', Items.stick);

		// try {
		// // Load up the research.
		// // TODO fix this
		// Research.load(SnowTech.configPath + File.separator
		// + "research_tree.xml");
		// } catch (ParserConfigurationException e) {
		// System.out.println("COULDN'T LOAD RESEARCH");
		// e.printStackTrace();
		// // Close with an error.
		// System.exit(1);
		// } catch (IOException e) {
		// System.out.println("COULDN'T LOAD RESEARCH");
		// e.printStackTrace();
		// // Close with an error.
		// System.exit(1);
		// } catch (SAXException e) {
		// System.out.println("COULDN'T LOAD RESEARCH");
		// e.printStackTrace();
		// // Close with an error.
		// System.exit(1);
		// }
	}

	public static void registerEntities() {
		int modEntityId = 0;
		EntityRegistry.registerModEntity(EntityUnclipperBullet.class,
				"Unclipper Bullet", ++modEntityId, SnowTech.instance, 64, 10,
				true);
	}
}
