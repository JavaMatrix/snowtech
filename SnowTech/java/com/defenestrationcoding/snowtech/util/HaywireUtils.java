package com.defenestrationcoding.snowtech.util;

import java.util.ArrayList;
import java.util.List;

import com.defenestrationcoding.snowtech.IHaywireDevice;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

/**
 * This class contains helper functions for Haywire interactivity.
 * 
 * @author JavaMatrix
 * 
 */
public class HaywireUtils {

	/**
	 * Performs a Haywire update. Currently uses a "pushing" style algorithm. In
	 * the future, this will be changed to a "pulling" style algorithm.
	 * 
	 * @param world
	 *            The world that the update should occur in.
	 * @param x
	 *            The x coordinate to update.
	 * @param y
	 *            The y coordinate to update.
	 * @param z
	 *            The z coordinate to update.
	 */
	public static void doHWUpdate(World world, int x, int y, int z) {
		// We'll need this to be able to update it.
		TileEntity te = world.getTileEntity(x, y, z);

		if (!(te instanceof IHaywireDevice)) {
			// We need it to be a haywire device to update it.
			return;
		}

		// Get the Haywire device version of the block.
		IHaywireDevice d = (IHaywireDevice) te;

		// Generate a list of neighbors. It would be much better
		// if I could figure out how to do this iteratively, but I've
		// had no idea how to do that yet.
		List<TileEntity> neighbors = new ArrayList<TileEntity>();
		neighbors.add(world.getTileEntity(x + 1, y, z));
		neighbors.add(world.getTileEntity(x - 1, y, z));
		neighbors.add(world.getTileEntity(x, y + 1, z));
		neighbors.add(world.getTileEntity(x, y - 1, z));
		neighbors.add(world.getTileEntity(x, y, z + 1));
		neighbors.add(world.getTileEntity(x, y, z - 1));

		// Iterate through the neighbors, and pull in the best signal
		int bestSignal = -1;
		long bestRank = Long.MIN_VALUE;
		
		for (TileEntity n : neighbors) {
			// Don't transfer signals from non-haywire stuff or from null.
			if (n == null || !(n instanceof IHaywireDevice)) {
				continue;
			}
			// Cast the broadcaster to a haywire device.
			IHaywireDevice broadcaster = (IHaywireDevice) n;
			if ( broadcaster.getHWSignal() < 0) {
				// Only take 0+ signals.
				System.out.println("Getting a negative signal from " + broadcaster.getClass().getSimpleName());
				continue;
			}

			// Now compare the signal
			if (broadcaster.getUpdateRank() >= bestRank) {
				bestRank = broadcaster.getUpdateRank();
				bestSignal = broadcaster.getHWSignal();
			}
		}
		d.setHWSignal(bestSignal);
	}
}
