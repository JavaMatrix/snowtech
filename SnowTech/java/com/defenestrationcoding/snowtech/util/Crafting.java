package com.defenestrationcoding.snowtech.util;

import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;

import com.defenestrationcoding.snowtech.research.Research;
import com.defenestrationcoding.snowtech.research.ResearchItem;

/**
 * A class for useful functions related to crafting.
 * @author JavaMatrix
 *
 */
public class Crafting {

	/**
	 * Removes all crafting recipes that shouldn't be allowed without
	 * researching them first.
	 */
	public static void handleResearchCrafting() {
		// Iterate through all research items.
		for (ResearchItem r : Research.getAllResearch()) {
			// Check if it's complete, and don't remove tier 0 recipes.
			if (!r.isComplete() && r.tier != 0) {
				// If not, iterate through all the items
				for (ItemStack i : r.items) {
					// And remove the recipes,
					List<IRecipe> recipes = RecipeRemover.removeAnyRecipe(
							i.getItem(), i.getItemDamage());
					// Storing them in the research item.
					r.recipes.addAll(recipes);

					// Check for smelting recipes, too.
					if (FurnaceRecipes.smelting().getSmeltingList()
							.containsKey(i)) {
						r.smelts.put(i, FurnaceRecipes.smelting()
								.getSmeltingResult(i));
						RecipeRemover.removeFurnaceRecipe(i);
					}
				}
			}
		}
	}
}
