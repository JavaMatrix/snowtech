package com.defenestrationcoding.snowtech.items;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.defenestrationcoding.snowtech.projectiles.EntityUnclipperBullet;

/**
 * Unclips entities from the world.  It's fun.
 * @author JavaMatrix
 *
 */
public class ItemUnclipperGun extends Item {
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List lore,
			boolean par4) {
		lore.add("Enemies tend to resist");
		lore.add("less when they don't");
		lore.add("collide with the ground.");
		lore.add("(Right-click to fire)");
	}
	
	@Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
		// Don't shoot if it's out of energy.
		if (stack.getItemDamage() >= stack.getMaxDamage()) {
			return stack;
		}
		
		// Damage the item first.
		stack.setItemDamage(stack.getItemDamage() + 1);
		
		// Shoot a bullet!
		if (!world.isRemote) {
			world.spawnEntityInWorld(new EntityUnclipperBullet(world, player));
		}
		
		return stack;
	}
}
