package com.defenestrationcoding.snowtech.items;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

import com.defenestrationcoding.snowtech.SnowTech;
import com.defenestrationcoding.snowtech.util.Registration.SnowTechItems;

/**
 * Zaps nearby entities when worn by the player.
 * 
 * @author JavaMatrix
 * 
 */
public class ItemDeathPack extends ItemArmor {

	/**
	 * For debugging/future config purposes.
	 */
	private static final boolean disableFeatures = false;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List lore,
			boolean par4) {
		if (stack.getItem().equals(SnowTechItems.deathPack)) {
			lore.add("This mighty weapon can be");
			lore.add("hooked to your back and");
			lore.add("will zap your enemies");
			lore.add("with lightning!");
		} else if (stack.getItem().equals(SnowTechItems.stepBoots)) {
			lore.add("These boots will help you");
			lore.add("travel by increasing your");
			lore.add("step height.");
		} else {
			System.out.println("ubererror 97: " + stack.getItem());
		}
	}

	public ItemDeathPack(ArmorMaterial armor, int slot) {
		super(armor, 1, slot);
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot,
			String type) {
		return "snowtech:textures/model/armor/deathpack_layer_1.png";
	}

	@Override
	public void onUpdate(ItemStack stack, World world, Entity e, int meta,
			boolean something) {
		// The step boot's step removal features go here, since this is only
		// called when the item is in the player inventory.
		if (armorType == 3 && e instanceof EntityPlayer) {
			((EntityPlayer) e).stepHeight = 0.5f;
		}
	}

	@Override
	public boolean onDroppedByPlayer(ItemStack stack, EntityPlayer e) {
		if (armorType == 3) {
			e.stepHeight = 0.5f;
			e.capabilities.setPlayerWalkSpeed(0.3f);
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onArmorTick(World world, EntityPlayer player, ItemStack stack) {
		// The death pack features go here.
		if (armorType == 1) {
			// Don't do the features if we haven't been told to.
			if (disableFeatures) {
				return;
			}

			// Make a box around the player's position.
			AxisAlignedBB aabb = AxisAlignedBB.getBoundingBox(player.posX - 4,
					player.posY - 4, player.posZ - 4, player.posX + 4,
					player.posY + 4, player.posZ + 4);

			// List out the mobs and the wolves.
			List<EntityLiving> mobs = world.getEntitiesWithinAABB(
					EntityMob.class, aabb);
			List<EntityWolf> wolves = world.getEntitiesWithinAABB(
					EntityWolf.class, aabb);
			// For some reason, Slimes aren't recognized as monsters.
			mobs.addAll(world.getEntitiesWithinAABB(EntitySlime.class, aabb));

			// Kill all enemies in the radius with a health of less than 200 (to
			// exclude bosses).
			for (EntityLiving enemy : mobs) {
				if (enemy.getHealth() > 0 && !(enemy.getHealth() >= 200)) {
					murder(world, enemy, player, stack);
				}
			}

			// Kill the angry wolves too.
			for (EntityWolf wolf : wolves) {
				if (wolf.isAngry()) {
					if (wolf.getHealth() > 0) {
						murder(world, wolf, player, stack);
					}
				}
			}
			return;
		}

		// Do speed boot stuff. This should probably be (re)moved. #SoftTodo
		if (armorType == 3) {
			player.stepHeight = 1;
			player.capabilities.setPlayerWalkSpeed(0.5f);
			return;
		}
	}

	/**
	 * Kills the entity using electricity from this pack.
	 * 
	 * @param world
	 *            The world containing the player who wears this pack.
	 * @param enemy
	 *            The enemy to murder.
	 * @param player
	 *            The player wearing this pack.
	 * @param stack
	 *            The itemstack containing this DeathPack.
	 */
	public void murder(World world, EntityLiving enemy, EntityPlayer player,
			ItemStack stack) {
		// Don't do this if we're out of energy.
		if (stack.getItemDamage() >= stack.getMaxDamage()) {
			return;
		}

		// Determine the enemy's health.
		float damage = enemy.getHealth();
		// Kill the enemy, or do as much damage as possible.
		enemy.attackEntityFrom(DamageSource.causePlayerDamage(player),
				stack.getMaxDamage() - stack.getItemDamage());
		// Play dat ZAP sound!
		world.playSoundAtEntity(player, SnowTech.modid + ":zap",
				1.0f + (float) Math.random(),
				(float) Math.pow(2, Math.random()));

		// Damage the item if the player isn't in creative.
		if (!player.capabilities.isCreativeMode) {

			int durability = (int) (damage);

			stack.setItemDamage(stack.getItemDamage() + durability);
			if (stack.getItemDamage() < 0) {
				stack.setItemDamage(0);
			}
		}
	}
}
