package com.defenestrationcoding.snowtech.items.hardtools;

import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.EnumHelper;

import com.defenestrationcoding.snowtech.items.IHardenedItem;

public class ItemHardenedMagicalShovel extends ItemSpade implements IHardenedItem {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addInformation(ItemStack stack, EntityPlayer player, List lore,
			boolean par4) {
		lore.add("\u00a76Half of Yueyachan!");
	}

	public ItemHardenedMagicalShovel() {
	super(EnumHelper.addToolMaterial("hardenedMagicalShovel", 3, 100, 12.0f, 2.5f,
			22));
	}

	@Override
	public int getMaxDamage(ItemStack stack) {
		if (!stack.hasTagCompound()) {
			return 1;
		} else if (!stack.getTagCompound().hasKey("maxDurability")) {
			return 1;
		}
		
		return stack.getTagCompound().getInteger("maxDurability");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void getSubItems(Item i, CreativeTabs tab, List relevantItems) {
		ItemStack stack = new ItemStack(this);
		NBTTagCompound nbt = new NBTTagCompound();
		if (stack.hasTagCompound()) {
			nbt = stack.getTagCompound();
		}
		nbt.setInteger("maxDurability", 2000);
		stack.setTagCompound(nbt);
		relevantItems.add(stack);
	}
}
