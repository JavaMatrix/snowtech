package com.defenestrationcoding.snowtech.items.hardtools;

import java.util.HashSet;
import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.EnumHelper;

import com.defenestrationcoding.snowtech.items.IHardenedItem;

public class ItemHardenedMetalHammer extends ItemTool implements IHardenedItem {
	@SuppressWarnings("rawtypes")
	public ItemHardenedMetalHammer() {
		super(0.0f, EnumHelper.addToolMaterial("hardenedMetalHammer", 3, 100, 0.0f, 2.5f,
				10), new HashSet());
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addInformation(ItemStack stack, EntityPlayer player, List lore,
			boolean par4) {
		lore.add("\u00a78Better than a precision sledgehammer!");
	}


	@Override
	public int getMaxDamage(ItemStack stack) {
		if (!stack.hasTagCompound()) {
			return 1;
		} else if (!stack.getTagCompound().hasKey("maxDurability")) {
			return 1;
		}

		return stack.getTagCompound().getInteger("maxDurability");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void getSubItems(Item i, CreativeTabs tab, List relevantItems) {
		ItemStack stack = new ItemStack(this);
		NBTTagCompound nbt = new NBTTagCompound();
		if (stack.hasTagCompound()) {
			nbt = stack.getTagCompound();
		}
		nbt.setInteger("maxDurability", 6000);
		stack.setTagCompound(nbt);
		relevantItems.add(stack);
	}
}
