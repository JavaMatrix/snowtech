package com.defenestrationcoding.snowtech.compat;

import net.minecraft.item.ItemStack;

import com.defenestrationcoding.snowtech.util.Registration.SnowTechItems;

import codechicken.nei.api.ItemInfo;

public class NEICompat {
	public static void load() {
		ItemInfo.hiddenItems.add(new ItemStack(SnowTechItems.moltenIngot));
	}
}
